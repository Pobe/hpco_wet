---
altLangPrefix: comptes-dutilisateur
dateModified: 2020-01-08
description:
  en: User Accounts
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: User Accounts
---

> ###### **Contents**
> 1. [User Accounts](#UserAccounts-userAccounts)
>     - [Personal Accounts](#UserAccounts-personalAccounts)
>     - [Shared Accounts](#UserAccounts-sharedAccounts)
>     - [Group Membership](#UserAccounts-groupMembership)
>     - [Storage](#UserAccounts-storage)
>     - [Passwords](#UserAccounts-passwords)
> 2. [Account Requests](#UserAccounts-accountsRequests)
>     - [Request a User Account](#UserAccounts-requestUserAccount)
>        - [Partner User Account](#UserAccounts-requestPartnerUserAccount)
>        - [Collaborator User Account](#UserAccounts-requestCollaboratoruserAccount)
>     - [Change Account Information](#UserAccounts-changeAccountInformation)
>     - [Passwords and Password Reset](#UserAccounts-passwordsReset)
>     - [Close a User Account](#UserAccounts-closeUserAccount)
>     - [Where to Submit](#UserAccounts-whereToSubmit)
>     - [Forms](#UserAccounts-forms)
> 3. [See Also](#UserAccounts-seeAlso)


# User Accounts
{: #UserAccounts-userAccounts}
All users **must** have a personal account. Generic accounts are **not** allowed.


### Personal Accounts
{: #UserAccounts-personalAccounts}

A personal account is created for an individual user, and only that user. The account is tied to the individual **not** the group who requested the account. If a user changes groups, an update to the account information is **required** by the group being left and the group being joined.

### Shared Accounts
{: #UserAccounts-sharedAccounts}

A shared account is used when multiple users must access resources or perform operations under a non-personal account. Shared accounts should never be used as generic accounts or a substitute for a personal account. Shared accounts belong to the group.

### Group Membership
{: #UserAccounts-groupMembership}

A group allows for tracking of resource usage and access control (e.g., directory permissions) applied to one or more users.

A user account must be a member of at least one group; membership in more than one group is possible. Group membership is set when the account is created and changed when the user changes groups.



### Storage
{: #UserAccounts-storage}

The home storage for personal accounts is tied to the user. When a user changes group, the contents of the user home may need to be moved to another storage location owned by the new group.

All other storage that is available to the user is tied to the group "owning" the storage.


### Passwords
{: #UserAccounts-passwords}

Users are responsible for securing their passwords at all times and periodically changing them. Passwords are subject to expiration.



# Account Requests
{: #UserAccounts-accountsRequests}
### Request a User Account
{: #UserAccounts-requestUserAccount}

User accounts are created for partners (Government of Canada department, agency, ministry) who are responsible for their users and collaborators. Any issues that arise will be raised with the applicant/user, the manager, and/or the authorized signer/representative.

>Accounts on science.gc.ca and collab.science.gc.ca are distinct and authorization is required for each. Having an account on one of the networks does not guarantee that one has an account on the other.


#### **Partner User Account**
{: #UserAccounts-requestPartnerUserAccount}
* Account requests **must** provide all information requested. If something is missing, the request will be rejected/sent back.
* Account requests **must** be signed by an authorized signer: an individual who is registered with HPCO and has authority to allocate resources (belonging to a group or a project).
* The Account Request form should be submitted with the New box checked.
    

    
#### **Collaborator User Account**
{: #UserAccounts-requestCollaboratoruserAccount}
Follow the same steps as described in Partner User Accounts with the following changes/clarifications:

* Applicant Information fields - the collaborator's user information.
* Department field - the partner sponsoring the collaborator.
* Manager Name field - the partner manager sponsoring the collaborator's account request and contact for dealing with collaborator issues.

### Change Account Information
{: #UserAccounts-changeAccountInformation}
An account update must be done when:

* adding access to a new system (e.g., adding access to gpscc)
* the user changes group
* the user's security situation changes (e.g., expires)

The Account Request form should be submitted with the Modify/Update box checked.


### Passwords and Password Reset
{: #UserAccounts-passwordsReset}
Once an account is created, the applicant will receive an email at the address provided on the account request form. The email will be encrypted using Entrust.

To request a password reset, send a service request using the normal process. The new password will be provided in the same way as above.


### Close a User Account
{: #UserAccounts-closeUserAccount}
User accounts are **not** deleted.

When a user account is closed, access to it is disabled and access to the home storage may be prevented (access disabled).

The Account Request form should be submitted with the Close box checked.


### Where to Submit
{: #UserAccounts-whereToSubmit}
See Getting Support for the process for submitting the Account Request form.


### Forms
{: #UserAccounts-forms}
* [EScienceAccountFormRequest-FormulaireDemandeCompteEScience.pdf](https://portal.science.gc.ca/confluence/download/attachments/39289394/EScienceAccountRequest-DemandeCompteEScience.pdf?version=1&modificationDate=1621530506141&api=v2)


# See Also
{: #UserAccounts-seeAlso}
* [Getting Support](./getting-support.html)





[back](./)
