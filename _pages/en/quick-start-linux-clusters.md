---
altLangPrefix: quick-start-linux-clusters
dateModified: 2020-01-08
description:
  en:  Quick Start to Using Linux Clusters With SLURM
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title:  Quick Start to Using Linux Clusters With SLURM
---

# Introduction

This tutorial describes how to use GPSC Linux clusters available on the Science Network using native SLURM commands. Topics include compiling, running and monitoring user programs.

It assumes:

* you already had your science account created (see Get an HPC Account)
* you already had your environment set up (see Setting up your Environment)
* you have an interactive session (see Interactive Access)

# Requirements

The default system gcc package is to be used.

The following openmpi package is to be used:

openmpi-4.1.1 hpcx-2.8.0

# Compile time

To compile an MPI program with gcc, only load the following MPI library:

`. ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

> If you are planning on using Intel compiler with MPI please first speak to your HPCO representative.

When using Intel compilers, load the correct compiler:

`. ssmuse-sh -x main/opt/intelcomp/intelpsxe-cluster-19.0.3.199`

`. ssmuse-sh -x main/opt/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199`

# Runtime

At run time load the following:

`. ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`


In the examples below, an image value is used for demonstration. Make sure to use the value that applies to you. Otherwise, your job will not run.

The container images available are found under the partner-specific pages. Start at:

[Partner Information](https://portal.science.gc.ca/confluence/display/SCIDOCS/Partner+Information)

and select the link that applies to you. Then, follow the links to gpsc → gpsc Container Images. You may then use/try one of the values listed.


# Examples

This section provides examples for:

* single threaded program
* MPI program - multi-node, single threaded
* OpenMP program - single node, multi threaded, shared memory
* combined OpenMP and MPI

Examples will be with sbatch and srun commands.

The main difference is that *srun* is interactive *and* blocking (you get the result in your terminal *and* you cannot write other commands until it is finished), while *sbatch* is batch processing *and* non-blocking (results are written to a file *and* you can submit other commands right away).


# Setup

Create the working directory:

`$ mkdir -p ~/tmp/quickstart`

> When creating your batch files or running your srun commands make sure to include the --account and --time directives which set the project and wall clock.  These two directives are mandatory.

# Single Threaded Program

This example shows how to create and submit a job for a single threaded program.

Create new file `hello.c`:

    /*

    ** hello.c

    */

    #include <stdio.h>

    int

    main(int argc, char **argv) {

    printf("hello\n");}

Compile with gcc:

`$ gcc -o hello hello.c`


Run the program interactively:


`$ ./hello`


Create job file `hello.sh`:

    #!/bin/bash -l

    #SBATCH --job-name=hello

    #SBATCH --output=hello.out

    #SBATCH --partition=standard

    #SBATCH --account=nrc_aero

    #SBATCH --time=00:00:60

    #SBATCH --ntasks=1

    #SBATCH --cpus-per-task=1

    #SBATCH --mem-per-cpu=2000M

    #SBATCH --comment="nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    cd ~/tmp/quickstart

    ./hello

| Directive                                                         	| Explanation                                                                                                                                                                                     	|
|-------------------------------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| #SBATCH --account=nrc_aero                                        	| Specify the project for accounting purposes.  To know which project you can submit to check,  gpsc - Gridengine - Projects within your  ministry.                                               	|
| #SBATCH --comment="nrc/nrc_all_default_ubuntu-18.04-amd64_latest" 	| The program will run inside this kind of  container. *Make sure it matches the partner  you belong to and the distribution you want*.                                                           	|
| #SBATCH --cpus-per-task=1                                         	| 1 cpu is required.                                                                                                                                                                              	|
| #SBATCH --job-name=hello                                          	| Modify job name as needed.                                                                                                                                                                      	|
| #SBATCH --mem-per-cpu=2000M                                       	| 2000MB is requested. This is a  reasonable minimum.                                                                                                                                             	|
| #SBATCH --ntasks=1                                                	| This option advises the Slurm controller  that job steps run whithin the allocation  will launch with a maximun number of 1 task  and to provide the sufficient resources. The  default is one. 	|
| #SBATCH --output=hello.out                                        	| Job output is sent to /hello.out                                                                                                                                                                	|
| #SBATCH --partition=standard                                      	| standard queue is chosen                                                                                                                                                                        	|
| #SBATCH --time=00:00:60                                           	| 60s of wallclock/run time to complete.                                                                                                                                                          	|

to run the batch file:

`$ sbatch hello.sh`


For launching job on specific cluster:

`$ sbatch --cluster=<clustername> hello.sh`


To run the job interactively:

`$ srun --account nrc_aero --partition standard --ntasks 1 --cpus-per-task 1 --mem-per-cpu 2000M --comment "nrc/nrc_all_default_ubuntu-18.04-amd64_latest" --time 00:00:60 --mpi none ./hello`


# MPI Hello World

This example shows how to create and submit a job for a multi node program using MPI.

Create new file `mpihello.c`:

    /*
    ** mpihello.c
    */

    #include <mpi.h>
    #include <stdio.h>
    #include <time.h>

    int main(int argc, char** argv) {

    // Initialize MPI.
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Sleep for random number of seconds. (1 to 5).
    // Use the thread_safe version of rand().
    int sleeptime;
    srand(time(NULL));
    sleeptime =  (rand_r() % 5) + 1;
    sleep(sleeptime);

    // Get the current time, with millisecond precision.
    struct timespec mytime;
    if (gettimeofday(&mytime, NULL) == 0)
    // Output process identity and sleep time.
    printf("Hello from processor %s, rank %d out of %d processors! I slept %d seconds until %d nanoseconds past %s",
    processor_name, world_rank, world_size, sleeptime, mytime.tv_nsec, ctime(&mytime.tv_sec));
    else
    // Output error message.
    printf("Hello from processor %s, rank %d out of %d processors! Sorry, I could not access the clock.\n",
    processor_name, world_rank, world_size);

    // Finalize the MPI environment. No more MPI calls allowed.
    MPI_Finalize();

    return(0);
    }

To build an MPI program, compiler wrappers are used:

First load the openmpi package:

`$ . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

`mpicc` calls underlying compiler (`gcc` in this case) with the required MPI include and library files.

`$ mpicc -o mpihello mpihello.c`

Create job file `mpihello.sh`

    #!/bin/bash -l
    #SBATCH --export=USER,LOGNAME,HOME,MAIL,PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    #SBATCH --job-name=mpihello
    #SBATCH --output=mpihello.out
    #SBATCH --account=nrc_aero
    #SBATCH --partition=standard
    #SBATCH --time=00:10:00
    #SBATCH --nodes=2
    #SBATCH --ntasks=8
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export SLURM_EXPORT_ENV=ALL

    # load mpi environment
    . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc
    . ssmuse-sh -x main/opt/openmpi-setup/openmpi-setup-0.3-hcoll

    cd ~/tmp/quickstart
    mpirun ./mpihello


Job directives explained:

| Directive                                     	| Explanation                                                                                                                                                                                 	|
|-----------------------------------------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| #SBATCH --time=00:10:00                       	| 10 minutes to complete.                                                                                                                                                                     	|
| #SBATCH --ntasks=8                            	| This option advises the SLURM controller that job steps run within the allocation will launch with a maximum number of 8 tasks and to provide the sufficient resources. The default is one. 	|
| #SBATCH --cpus-per-task=8                     	| 8 cpus per slot. This gives a total of 8x8=64 cpus to run the job                                                                                                                           	|
| #SBATCH --mem-per-cpu=4000M                   	| 4000MB per slot; this gives a total of 2x4000=8000 MB to run the job                                                                                                                        	|
| #SBATCH --nodes=2                             	| 2 nodes are required                                                                                                                                                                        	|
| #SBATCH --export=USER,LOGNAME,HOME ...        	| changes the SLURM_EXPORTENV environment variable. Doing export SLURM_EXPORT_ENV=ALL sets the variable to all when we launch the MPI program.                                                	|


to run the batch job:

`$ sbatch mpihello.sh`

# OpenMP Hello World

This example shows how to create and submit a job for a multi threaded program using OpenMP.

Create new file `omphello.c`:

    /*
    *omphello.c
    */

    #include <omp.h>
    #include <stdio.h>
    #include <stdlib.h>

    int
    main(int argc, char **argv) {
    int tid;

    #pragma omp parallel private(tid)
    {
    tid = omp_get_thread_num();
    printf("Hello World from thread = %d\n", tid);
    if (tid == 0) {
    printf("Number of threads (%d)\n", omp_get_num_threads());
    printf("OMP_NUM_THREADS (%s)\n", getenv("OMP_NUM_THREADS"));
    }
    }
    exit(0);
    }

To build an OpenMP program, specify the required compiler-specific option:

`$ gcc -fopenmp -o omphello omphello.c`

To run, set the `OMP_NUM_THREADS` environment variable (though not required, `OMP_NUM_THREADS` <= number of cpus available):

`$ export OMP_NUM_THREADS=4`
`$ ./omphello`

Create job file `omphello.sh` (make sure to set `OMP_NUM_THREADS`):

    #!/bin/bash -l
    #SBATCH --job-name=omphello
    #SBATCH --output=omphello.out
    #SBATCH --partition=standard
    #SBATCH --account=nrc_aero
    #SBATCH --time=00:00:60
    #SBATCH --nodes=1
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export OMP_NUM_THREADS=4
    # run program
    cd ~/tmp/quickstart
    ./omphello

Submit job:

`$ sbatch omphello.sh`

If the `OMP_NUM_THREADS` is *not* set, the program will try to determine the number cpus available and use that many threads. If a job runs in a container, then not specifying the `OMP_NUM_THREADS` will be work but more cpus than expected may be used. It is usually best to set `OMP_NUM_THREADS`.

submit job interactively:

`$ export OMP_NUM_THREADS=4`
`$ srun --account nrc_aero --partition standard --nodes 1 --cpus-per-task 8 --mem-per-cpu 4000M --comment "nrc/nrc_all_default_ubuntu-18.04-amd64_latest" --time 00:00:60 mpirun ./omphello`

# OpenMP+MPI Hello World

This code contains parts of `mpihello.c` and `omphello.c`.

Create file `mpiomphello.c`:

    /*
    ** mpiomphello.c
    */

    #include <mpi.h>
    #include <omp.h>
    #include <stdio.h>
    #include <time.h>

    int main(int argc, char** argv) {
    int iam = 0, np = 1;

    // Initialize MPI.
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Sleep for random number of seconds. (1 to 5).
    // Use the thread_safe version of rand().
    int sleeptime;
    srand(time(NULL));
    sleeptime =  (rand_r() % 5) + 1;
    sleep(sleeptime);

    // Get the current time, with millisecond precision.
    struct timespec mytime;
    if (gettimeofday(&mytime, NULL) == 0)
    // Output process identity and sleep time.
    printf("Hello from processor %s, rank %d out of %d processors! I slept %d seconds until %d nanoseconds past %s",
    processor_name, world_rank, world_size, sleeptime, mytime.tv_nsec, ctime(&mytime.tv_sec));
    else
    // Output error message.
    printf("Hello from processor %s, rank %d out of %d processors! Sorry, I could not access the clock.\n",
    processor_name, world_rank, world_size);

    #pragma omp parallel default(shared) private(iam, np)
    {
    np = omp_get_num_threads();
    iam = omp_get_thread_num();
    printf("Hello from OMP thread %d out of %d from MPI process %d out of %d on %s\n",
    iam, np, world_rank, world_size, processor_name);
    }


    // Finalize the MPI environment. No more MPI calls allowed.
    MPI_Finalize();

    return(0);
    }

Build using the `mpicc` wrapper with the required option for OpenMP:

`$ . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

Note: the `gcc -fopenmp` option is compiler-specific.

`$ mpicc -fopenmp mpiomphello.c -o mpiomphello`


Create job `mpiomphello.sh` file:

    #!/bin/bash -l
    #SBATCH --export=USER,LOGNAME,HOME,MAIL,PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    #SBATCH --job-name=mpiomphello
    #SBATCH --output=mpiomphello.out
    #SBATCH --partition=standard
    #SBATCH --account=nrc_aero
    #SBATCH --time=00:00:60
    #SBATCH --nodes=4
    #SBATCH --ntasks=4
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export SLURM_EXPORT_ENV=ALL

    # load mpi environment
    . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc
    . ssmuse-sh -x main/opt/openmpi-setup/openmpi-setup-0.3-hcoll

    export OMP_NUM_THREADS=8
    cd ~/tmp/quickstart
    mpirun -n 4 ./ompmpihello


    #to run the script with srun command
    srun ./ompmpihello


Submit job:

`$ sbatch mpiomphello.sh`

# Job Monitoring

There are two ways you can monitor your jobs with Slurm.

## squeue

When doing:

`$ squeue -u <username>`

A list of jobs that are in the queue for the given username with jobid, name, state and other useful information will be displayed.

## sacct

sacct gives information about jobs that were launched on a given cluster from specific time frames and can give more useful information based on command parameters.

An example of such a command is:

`$ sacct -a -p S 2020-01-11 E 2021-01-11 --format jobid,start,end,state,ReqNodes,Account,Partition,ncpus,ReqMem,group,JobName,user,timelimitRaw `

This command lists the jobs that were launched between 2020-01-11 and 2021-01-11 with the listed parameters outputted as columns.

For more information on specific output fields see https://slurm.schedmd.com/sacct.html .

