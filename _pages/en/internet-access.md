---
altLangPrefix: internet-access
dateModified: 2020-01-08
description:
  en: Internet Access from the Science and Collab Science Networks
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Internet Access from the Science and Collab Science Networks
---

# Introduction

Access to the Internet from the science.gc.ca network is not uniform:

- Interactive containers - can access the Internet as normal.
- Jobs running on the clusters - run in part of the science network which has limited access to the Internet.

> Staging Data Is Recommended.
As the clusters are meant for high performance computing, it is highly recommended that all required data is available local to the science network rather than pulled in from within a job. There are times when this practice can be relaxed, but, any operations that spend significant amounts of time waiting for services external to the science network may be needlessly costly.

# Web Services

A proxy server is available to enable access to the web (http and https only) from jobs running on cluster nodes

For science.gc.ca:

    export http_proxy=http://webproxy.science.gc.ca:8888/

    export https_proxy=http://webproxy.science.gc.ca:8888/

For collab.science.gc.ca:

    export http_proxy=http://webproxy.collab.science.gc.ca:8888/

    export https_proxy=http://webproxy.collab.science.gc.ca:8888/

Note: The proxy is *not* necessary when running in interactive containers.

# FTP Access

There is currently no support for accessing FTP servers from jobs running on the clusters.