---
altLangPrefix: mettre-en-place-ssh
dateModified: 2020-01-08
description:
  en: Setting up SSH
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Setting up SSH
---

> ###### **Contents**
> 1. [Introduction](#SettingUpssh-introduction)
> 2. [ssh Keys](#SettingUpssh-sshKeys)
>     - [Procedure](#SettingUpssh-procedure)
> 3. [ssh Configuration File](#SettingUpssh-sshConfigurationFile)
>     - [Required Settings](#SettingUpssh-requiredSettings)
> 4. [Other Configurations](#SettingUpssh-otherConfigurations)
>     - [Avoid Dropped Idle Connections](#SettingUpssh-avoidDroppedIdleConnections)
>     - [X11 Support](#SettingUpssh-x11Support)

# Introduction
{: #SettingUpssh-introduction}

ssh is an indispensable tool that is used explicitly, and behind the scenes (e.g., by the MPI launcher).

For many uses of ssh, it is necessary to avoid interaction (e.g., to enter password, enter passphrase, or enter yes). This is true for this site also.

# ssh Keys
{: #SettingUpssh-sshKeys}


Passwordless ssh is required to access services and submit jobs. This is done by use private and public keys.

> **RSA Keys Required**
> RSA keys are now mandatory. Do not expect DSA keys to work.

# Procedure
{: #SettingUpssh-procedure}

1. Create .ssh directory in $HOME directory:
```
mkdir -p ~/.ssh
```

2. Set permission:
```
chmod -R 700 ~/.ssh
```

3. Change directory:
```
cd ~/.ssh
```

4. Generate ssh key:
```
ssh-keygen -q -t rsa
```
Note: _Unless you have specific reasons not to, let ssh-keygen use the suggested key file name (id_rsa)._

5. Add your public key to your authorized keys file.
For passwordless login using the same account (this is required to access and submit jobs on the HPC science network):
```
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

6. (Optional) For passwordless login between different user accounts, you need to copy the source account's public key to the destination account first:
```
scp ~/.ssh/id_rsa.pub <destination username>@<hostname>:~
ssh <destination username>@<hostname>
cat ~/id_rsa.pub >> ~/.ssh/authorized_keys
rm -rf ~/id_rsa.pub
```

# ssh Configuration File
{: #SettingUpssh-sshConfigurationFile}

## Required Settings
{: #SettingUpssh-requiredSettings}

Add to `~/.ssh/config`:
```
Host *.science.gc.ca
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null

Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
```
# Other Configurations
{: #SettingUpssh-otherConfigurations}

## Avoid Dropped Idle Connections
{: #SettingUpssh-avoidDroppedIdleConnections}

When connecting to the Science network, an idle connection may be dropped for various reasons. The problem may be at **any point** along the connection.

If you are experiencing this, then try adding to `~/.ssh/config` on the **client** side:
```
# avoid dropped idle connections
ServerAliveInterval 60
```

## X11 Support
{: #SettingUpssh-x11Support}

To enable X11 support by add to `~/.ssh/config`:
```
ForwardX11 yes
```

This should be done:

* for your HPC account
* on your desktop client
