---
altLangPrefix: mirroring tool
dateModified: 2020-01-08
description:
  en: mirroring tool
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Mirroring Tool
---

# Introduction

From the `rmirr` home page:

`rmirr` provides a *safe* and easy to use way to set up and execute file storage mirroring instructions. And because it uses `rsync` to actually perform the synchronization, you get all the performance and availability benefits of `rsync`.


# To Load

`. ssmuse-sh -x main/opt/rmirr/rmirr-0.5`

To use, `rmirr` requires a configuration file at `~/.rmirr/rmirr.json`. See the documentation for details.


# Reference

* [rmirr home page](https://expl.info/bin/view/Projects/RMIRR)
