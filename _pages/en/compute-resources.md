---
altLangPrefix: compute resources
dateModified: 2020-01-08
description:
  en: compute resources
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Compute Resources
---

There are a number HPC systems available for batch and interactive use. Systems for batch use are available to run serial jobs or parallel jobs (MPI and OpenMP). Systems dedicated to interactive use are available to run graphical tools, to compile, debug, submit batch jobs and to run non-compute intensive jobs without scheduling.


| System   	| Network             	| Batch 	| Interactive 	| Batch System 	|
|----------	|---------------------	|-------	|-------------	|--------------	|
| gpsc-in  	| science             	|       	| X           	| N/A          	|
| gpsc-vis 	| science             	|       	| X           	| N/A          	|
| gpsc1    	| science             	| X     	|             	| Gridengine   	|
| gpsc2    	| science             	| X     	|             	| Gridengine   	|
| gpsc3    	| science             	| X     	|             	|              	|
| gpsc4    	| science             	| X     	|             	| Gridengine   	|
| gpscc2   	| collab.science      	| X     	|             	| Gridengine   	|
| ppp3     	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| ppp4     	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| banting  	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| daley    	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
