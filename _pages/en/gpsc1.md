---
altLangPrefix: gpsc1
dateModified: 2020-01-08
description:
  en: gpsc1
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: gpsc1
---


# Description

Gpsc1 (General Purpose Science Cluster) is a Linux High Performance Computing System made available to support scientific computing needs across various government departments.

The gpsc1 is composed of head nodes, compute nodes, large nodes and GPU nodes.  There is no interactive access to the head and compute nodes.  Gpsc1 implements containers to provide isolation and to accommodate different user environments.  User jobs are submitted from gpsc-in and run in batch within containers in a chosen available OS (Ubuntu, CentOS, openSUSE, RedHat) on the compute nodes.  Each batch job container is accessible interactively.  The GPSC uses a batch system to distribute the workload within containers on the cluster.


# Specification

|                      	| Head Nodes                         	| Compute Nodes                      	| Large Compute Nodes                      	| Super Large Compute Nodes             	| GPU (v100) Compute Nodes                      	|
|----------------------	|------------------------------------	|------------------------------------	|---------------------------------------	|---------------------------------------	|-----------------------------------------------	|
| Model                   	| 2U Dell PowerEdge R720 rack server 	| Dell  PowerEdge M620 blade server  	| 2U Dell  PowerEdge  R820 rack  server 	| 2U Dell  PowerEdge  R820 rack  server 	|                                               	|
| # Nodes                 	| 3                                  	| 256                                	| 10                                    	| 9                                     	| 3                                             	|
| CPUs (per node)         	| 2 x Intel Xeon E5-2650v2 @ 2.60GHz 	| 2 x Intel Xeon E5-2650v2 @ 2.60GHz 	| 4 x Intel Xeon E5-4650v2 @ 2.40GHz    	| 4 x Intel Xeon E7-8860v3 @ 2.20GHz    	| Intel(R)  Xeon(R)  Gold  6140 CPU  @  2.30GHz 	|
| Cores per node          	| 16                                 	| 16                                 	| 40                                    	| 64                                    	| 36                                            	|
| CPU threads per node    	| 16                                 	| 32                                 	| 80                                    	| 128                                   	| 72                                            	|
| Memory per node         	| 384 GB                             	| 128 GB                             	| 1 TB                                  	| 3 TB                                  	| 1024                                          	|
| Total Cores             	| 48                                 	| 4096                               	| 400                                   	| 576                                   	| 108                                           	|
| GPUs per Node           	|                                    	|                                    	|                                       	|                                       	| 4                                             	|
| Total GPUs              	|                                    	|                                    	|                                       	|                                       	| 12                                            	|
| Network                 	| InfiniBand FDR                     	| InfiniBand FDR                     	| InfiniBand FDR                        	| InfiniBand FDR                        	| InfiniBand FDR                                	|
| Base OS                 	| Ubuntu 18.04                       	| Ubuntu 18.04                       	| Ubuntu 18.04                          	| Ubuntu 18.04                          	| Ubuntu 18.04                                  	|
| Batch System            	| Gridengine                         	| Gridengine                         	| Gridengine                            	| Gridengine                            	| Gridengine                                    	|


Notes:

* for compute nodes, hyperthreading accounts for two CPU threads per core, but only one CPU thread per core is allocated by the queueing system.
* available memory is always less and dependent on the queueing system
* [http://ark.intel.com/products/75269/Intel-Xeon-Processor-E5-2650-v2-20M-Cache-2_60-GH](http://ark.intel.com/products/75269/Intel-Xeon-Processor-E5-2650-v2-20M-Cache-2_60-GH)
* [https://en.wikipedia.org/wiki/InfiniBand](https://en.wikipedia.org/wiki/InfiniBand)

# Queueing System
## Resources

| PE       	| Queue                         	| # Nodes 	| # Cpus   	| Memory 	|   	| Gpu Type 	|
|----------	|-------------------------------	|---------	|----------	|--------	|---	|----------	|
| dev      	| dev-be, dev-be-6h             	| 240     	| 100000M  	|        	|   	|          	|
| dev-In   	| dev-ln, dev-ln-6h             	| 10      	| 980000M  	|        	|   	|          	|
| dev-sln  	| dev-sln, dev-sln-6h           	| 9       	| 2800000M 	|        	|   	|          	|
| gpu-v100 	| dev-gpu-v100, dev-gpu-v100-6h 	| 3       	| 980000M  	| 4      	|   	| v100     	|

Notes:

* jobs should submit to PEs (`-pe <pename> <nslots>`), not queues
* resources available may not match hardware resources
* select gpu type using `res_gputype` (e.g., `-l res_gputype=v100`)

# Limits

| Type              	| Limit     	|
|-------------------	|-----------	|
| slots             	| 200/user  	|
| cpus (`res_cpus`) 	| 2048/user 	|

Table Header-1 | Table Header-2
:--- | ---:
Table Data-1 | Table Data-2
TD-4 | Td-5
Table Data-7 | Table Data-8
