---
altLangPrefix: obtenir-de-l'aide
dateModified: 2020-01-08
description:
  en: getting-support
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Getting Support
---

> ###### **Contents**
> 1. [Support Representative](#GettingSupport-supportRepresentative)
> 2. [Process](#GettingSupport-process)
> 3. [Mailing Lists](#GettingSupport-mailingLists)
> 4. [Contacts](#GettingSupport-contacts)

# Support Representative
{: #GettingSupport-supportRepresentative}
There is an HPCO Partner Support Representative assigned to each partner who is the primary resource for getting support. The support representative provides information, training, guidance, and is responsible for managing issues and service requests.


# Process
{: #GettingSupport-process}
The process for getting support is:

1. Ask your local user representative who is the first line for help.
2. Submit your issue/request to your local help desk (who in turn will submit it to the SSC service desk). Send a carbon copy (CC) to [ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca](mailto:ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca). Somewhere in the request to service desk, add:

> Please assign to the SSC/HPCO group with Owner Group DC000134.    
> Please ensure that any/all attachments are copied.

For questions that may be **easily** and **quickly** answered:

1. Email your partner support representative, or
2. Email HPCO Service Inbox ([hpcoptimizationservice-serviceoptimisationchp@ssc-spc.gc.ca](mailto:hpcoptimizationservice-serviceoptimisationchp@ssc-spc.gc.ca)).

# Mailing Lists
{: #GettingSupport-mailingLists}
Mailing lists are set up for partners and projects and are used for communication from the support representative to the partner and project users.

Please do not use these mailing lists for general support. Follow the process in the above section instead.
# Contacts
{: #GettingSupport-contacts}


| Partner/Project    | Representative                  | Backup          | Email List                                                                |
|--------------------|:--------------------------------|:----------------|:----------------------------------------------------------------------------|
| AAFC               | Bernie Genswein                 |                 | [Link](mailto:hpco-aafc-partner-support-ochp-aac-soutien-au-partenaire@lists.ec.gc.ca)     |
| CFIA               | Bernie Genswein                 | John Marshal    |                                                                             |
| CSA                | Michel Béland                   |                 |                                                                             |
| DFO                | John Marshall / Bernie Genswein |                 | [Link](mailto:hpco-dfo-partner-support-ochp-mpo-soutien-au-partenaire@lists.ec.gc.ca)      |
| ECCC (HPC-MAC)     | Michel Béland                   | Alain Desmeules | [Link](mailto:hpc-user-reps@lists.ec.gc.ca)(broken at the moment)                         |
| ECCC (non HPC-MAC) | Alain Desmeules                 | Michel Béland   |                                                                             |
| GRDI               | Bernie Genswein                 |                 |                                                                             |
| MC                 |                                 |                 |                                                                             |
| NRC                | John Marshall / Moussa Abdenbi  | Michel Béland   | [Link](mailto:hpco-nrc-partner-support-ochp-cnrc-soutien-au-partenaire@lists.ec.gc.ca)     |
| NRCAN              | Antoine LeBel                   | John Marshall   | [Link](mailto:hpco-nrcan-partner-support--ochp-rncan-soutien-au-partenaire@lists.ec.gc.ca) |
| PHAC               | John Marshall                   |                 |                                                                             |
| StatCan            | Moussa Abdenbi                  |                 |                                                                             |
