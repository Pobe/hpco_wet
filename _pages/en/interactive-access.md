---
altLangPrefix: acces interactif
dateModified: 2020-01-08
description:
  en: interactive access
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Interactive Access
---

> ###### **Contents**
> 1. [Introduction](#InteractiveAccess-introduction)
> 2. [ThinLinc](#InteractiveAccess-thinlinc)
> 3. [ssh / PuTTY](#InteractiveAccess-sshputty)
>     - [Landing Pad / Login Hosts](#InteractiveAccess-landingPadLoginHosts)
>     - [Selecting an Interactive Container](#InteractiveAccess-selectingContainer)
>     - [Connecting to an Interactive Container](#InteractiveAccess-connectingContainer)
>        - [Linux - ssh](#InteractiveAccess-connectingContainerLinux)
>        - [Windows - PuTTY](#InteractiveAccess-connectingWindowsPutty)
>     - [Connect with X11 Forwarding Enabled](#InteractiveAccess-connectWithX11)
>        - [Linux](#InteractiveAccess-connectWithX11Linux)
>     - [Windows - PuTTY](#InteractiveAccess-windowsPutty)

# Introduction
{: #InteractiveAccess-introduction}
Interactive access to the **Science** network is provided using:

* ThinLinc
* ssh / PuTTY

Interactive access to the **Collab Science** network is provided using:

* ssh / PuTTY

> The Science and Collab Science networks are distinct, separate, and wholly independent. Although account names may be the same on both, they **do not share** account settings, home filesystems, storage. Also, the Collab Science network is accessible from the Science network, but not the reverse.

# ThinLinc
{: #InteractiveAccess-thinlinc}
ThinLinc provides remote desktop environment. For more information see:

[ThinLinc - Remote Desktop and Visualisation](https://portal.science.gc.ca/confluence/display/SCIDOCS/ThinLinc+-+Remote+Desktop+and+Visualisation)

See Landing Pad below for the host to connect to.

# ssh / PuTTY
{: #InteractiveAccess-sshputty}
ssh / PuTTY provide an ssh and terminal session.

## Landing Pad / Login Hosts
{: #InteractiveAccess-landingPadLoginHosts}
The landing pad for the Science network is at:

* `sci-eccc-in.science.gc.ca` - CMC users only
* `gpsc-in-gccloud.science.gc.ca` -  External/all other users
* `gpsc-in.science.gc.ca` - Alternative if `gpsc-in-gccloud.science.gc.ca` does not answer

For all the examples below, `gpsc-in-gccloud.science.gc.ca` will be used as the landing pad.

The landing pad for the Collab Science network is at:

*  `gpscc-in.collab.science.gc.ca` - Accessible from the Internet.

## Selecting an Interactive Container
{: #InteractiveAccess-selectingContainer}
The environment of each user is pre-configured to connect to an interactive container that was specified on the account creation form.

Each partner has one or more dedicated interactive containers available for use. E.g.,

* `<partner>-interactive--centos-6.7-amd64-64`
* `<partner>-interactive--ubuntu-18.04-amd64-64`

where <partner> is dfo, nrc, nrcan, etc. E.g., for NRC users, the following interactive containers are available:

* `nrc-interactive--centos-6.7-amd64-64`
* `nrc-interactive--ubuntu-18.04-amd64-64`

> **Restricted Access**    
> Users must use their own partner-specific containers.

To set/modify the interactive container setting (using `nrc-interactive--ubuntu-18.04-amd64-64` as an example):
```
echo "nrc-interactive--ubuntu-18.04-amd64-64" > ~/.sshj/interactive/gpsc1.science.gc.ca/jobname
```
If the pre-configured setting is not correct, or you would like to change it, use one of your partner settings just described, or contact your local user representative or [ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca.](mailto:ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca
)

## Connecting to an Interactive Container
{: #InteractiveAccess-connectingContainer}
### **Linux - ssh**
{: #InteractiveAccess-connectingContainerLinux}
In a terminal, ssh to the landing pad (`gpsc-in-gccloud.science.gc.ca`) and enter your username and password:
```
ssh username@gpsc-in-gccloud.science.gc.ca
```

### **Windows - PuTTY**
{: #InteractiveAccess-connectingWindowsPutty}
1. Click on menu Start > Programs > PuTTY
2. In PuTTY:
      * Under Category > Session > Host Name, enter landing pad hostname (gpsc-in-gccloud.science.gc.ca), then click Open (Figure 1)
      * Enter your username and password

Figure 1:   
![Putty](../media/putty_gccloud.png){: .img-responsive}


## Connect with X11 Forwarding Enabled
{: #InteractiveAccess-connectWithX11}
X11 forwarding should already be enabled by default when your account was created.  Please check your `~/.ssh/config` to make sure this is the case.

### Linux
{: #InteractiveAccess-connectWithX11Linux}
In a terminal (e.g., using the landing pad host "gpsc-in-gccloud.science.gc.ca"):
```
ssh username@gpsc-in-gccloud.science.gc.ca
```

Add the following line to your file `~/.ssh/config`:
```
ForwardX11 yes
```

Start a new session with X11 Forwarding enabled:
```
ssh -X username@gpsc-in-gccloud.science.gc.ca
```

## Windows - PuTTY
{: #InteractiveAccess-windowsPutty}
Connect to the landing pad:

1. Click on menu Start > Programs > PuTTY
2. In PuTTY:
    * Under Category > Session > Host Name, enter the landing pad host ("gpsc-in-gccloud.science.gc.ca"), then click Open (Figure 1)
    * Enter your username and password

Add the following line to your file `~/.ssh/config`:
```
ForwardX11 yes
```

> In order to use X11 forwarding on Windows, you must have an X Server for Windows (e.g. Xming, MobaXterm) running on your local Windows workstation.

Using MobaXterm:

* Start a new session with X11 Forwarding enabled:
```
ssh -X username@gpsc-in-gccloud.science.gc.ca
```

Using Xming with PuTTY:

1. Start Xming
2. In PuTTY:
      * Under Category > Session > Connection > SSH > Section "X11 forwarding", check "Enable X11 forwarding" (Figure 2)
      * Under Category > Session > Host Name, enter the landing pad host ("gpsc-in-gccloud.science.gc.ca"), then click Open (Figure 1)
      * Enter your username and password

Figure 2: Enable X11 forwarding    
![Putty_x11](../media/putty_x11.png){: .img-responsive}



