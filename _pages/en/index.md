---
altLangPrefix: index
authorName: Service Canada
authorUrl:
contentTitle: Canada.ca Jekyll theme
dateModified: 2020-01-10
description:
  en: "Get quick, easy access to all Government of Canada services and information."
  fr: "Accédez rapidement et facilement à tous les services et renseignements du gouvernement du Canada."
noContentTitle: true
pageclass: wb-prettify all-pre
subject:
  en: [GV Government and Politics, Government services]
  fr: [GV Gouvernement et vie politique, Services gouvernementaux]
title: Test page - Canada.ca
---
- [Getting started](./getting-started.html)
- [User accounts](./user-accounts.html)
- [Setting up SSH](./setting-up-ssh.html)
- [Setting up the environment](./setting-up-the-environment.html)
- [Interactive access](./interactive-access.html)
- [Getting support](./getting-support.html)
- [Compute Resources](./compute-resources.html)
- [GPSC1](./gpsc1.html)
- [Remote Desktop and Visualisation](./remote-desktop-visualisation.html)
- [Quick Start to Using Linux Clusters with SLURM](./quick-start-linux-clusters.html)
- [Mirroring Tool](./mirroring-tool.html)
