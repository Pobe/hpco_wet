---
altLangPrefix: mettre-en-place-environnement
dateModified: 2020-01-08
description:
  en: setting up the environment
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Setting Up the Environment
---

> ###### **Contents**
> 1. [Introduction](#SettingUpTheEnvironment-introduction)
> 2. [Supported Shells](#SettingUpTheEnvironment-supportedShells)
> 3. [Latest Configuration](#SettingUpTheEnvironment-latestConfiguration)
> 4. [.profile vs .bashrc](#SettingUpTheEnvironment-pvb)
> 5. [See Also](#SettingUpTheEnvironment-seeAlso)

# Introduction
{: #SettingUpTheEnvironment-introduction}
The standard method for setting up the environment is accomplished by using the `ordenv` package. `ordenv` provides a way for site administrators, user (community and group) representatives, and users to configure the environment.

> A default setup should already have been done when your account was created.


# Supported Shells
{: #SettingUpTheEnvironment-supportedShells}
Although other shells may be used, support is provided for:

* bash
* ksh

# Latest Configuration
{: #SettingUpTheEnvironment-latestConfiguration}
Make sure that the following is in your `~/.profile`:
```
export ORDENV_SITE_PROFILE=20210129
export ORDENV_COMM_PROFILE=
export ORDENV_GROUP_PROFILE=
. /fs/ssm/main/env/ordenv-boot-20201118.sh
```
Notes:

* some standard packages are loaded: `ssmuse`, `ssm`, `ordenv`
* the site-specific profile is loaded according to the setting of `ORDENV_SITE_PROFILE`; this is applicable to **all** users
* the community-specific profile is loaded according to the setting of `ORDENV_COMM_PROFILE`; **consult your user representative for the setting or leave empty**
* the group-specific profile is loaded according to the setting of `ORDENV_GROUP_PROFILE`; **consult your user or group representative for the setting or leave empty**
* lastly, the user-specific environment is set up according to the contents of `~/.profile.d`; consult the [ordenv 4 User Guide](https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide) on how to configure

> If your environment does not seem to be configured as you expect, check that `ORDENV_COMM_PROFILE` and `ORDENV_GROUP_PROFILE` are correctly set.

# .profile vs .bashrc
{: #SettingUpTheEnvironment-pvb}
It is almost always preferable to use `~/.profile` to set up your shell environment. It is loaded automatically **for login shells but not otherwise**. This means that calls like `"ssh <host> date"` run with a minimal environment and, as a result, start almost immediately. Additions to the login shell configuration can be loaded into the current login session by running `. /etc/profile` and/or `. ~/.profile` if necessary.    

`~/.bashrc` is useful for settings that must be applied **whenever a new shell** is started, including for non-login shells. As such, it must be used judiciously and should typically not output anything to stdout.

# See Also
{: #SettingUpTheEnvironment-seeAlso}
* [ordenv 4 User Guide](https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide)

