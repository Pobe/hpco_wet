---
altLangPrefix: mise-en-route
dateModified: 2020-01-08
description:
  en: getting started
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Getting Started
---

> ###### **Contents**
> 1. [Get an Account](#GetAnAccount)
> 2. [Log into the Account](#LogInIntoTheAccount)
> 3. [Configuration](#Configuration)
>     - [Set Up ssh](#SetUpssh)
>     - [Set Up the User Environment](#SetUpTheUserEnvironment)
> 4. [Support](#Support)


## Get an Account
{: #GetAnAccount}

A personal account is required by all users of the HPC resources.


*   [User Accounts](./user-accounts.html)


## Log into the Account
{: #LogInIntoTheAccount}

An interactive container is waiting for you to log in. This is possible from Linux/UNIX and Windows machines.

*   [Interactive Access](./interacitve-access.html)

## Configuration
{: #Configuration}


An initial configuration should have be done at accounting creation to allow the user to log in with a password.

Further configuration is required by the user to use the HPC resources and get support. This configuration should be maintained to get the best service.

#### Set Up ssh
{: #SetUpssh}

A minimal configuration of ssh is required.

*   [Setting Up ssh](./setting-up-ssh.html)

#### Set Up the User Environment
{: #SetUpTheUserEnvironment}

A minimal configuration of the working environment is required.

*   [Setting Up the Environment](./setting-up-the-environment.html)

## Support
{: #Support}

If you have problems with the above step, please see:

*   [Getting Support](./getting-support.html)


[back to home](../index.html)