---
altLangPrefix: remote-desktop-visualisation
dateModified: 2020-01-08
description:
  en: remote desktop and visualisation
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Remote Desktop and Visualisation
---

# Introduction

Thinlinc is a remote desktop tool used by users outside of the science network to access a fully operational Linux Desktop Environment on the science network using their workstation.


# Client Installation

You have to install the ThinLinc client on your computer. Download it https://www.cendio.com/thinlinc/download. Choose the appropriate version for your computer.

> For users of the ECCC network only, the ThinLinc client is already installed and available through SSM using either:

> `. ssmuse-sh -x main/opt/thinlinc/thinlinc_4.11.0`

> or

> `. ssmuse-sh -d hpcs/ext/thinlinc-4.7.0`


# Server Installation

You do not need to install the ThinLinc server to use ThinLinc on gpsc-vis or hpcr-vis, where it is already installed. For those needing it, here are the installation instructions:

1. Download the ThinLinc server .deb package on the Cendio web site (https://www.cendio.com/thinlinc/download)

2. Install on a workstation with the following command (as root):

`dpkg -i $path/$package.deb`

3. Disable `firewalld`:

`sudo service firewalld stop`

4. Modify `/etc/locale.gen` like this:

`en_CA.UTF-8 UTF-8`
`en_US.UTF-8 UTF-8`
`fr_CA.UTF-8 UTF-8`

5. Restart `firewalld`:

`sudo service firewalld start`


# Client Setup and Connection

To start the client, run:

`tlclient`


# Connection

Server:

* `gpsc-vis.science.gc.ca` (everyone except most ECCC users)
* `hpcr-vis.science.gc.ca` (most ECCC users)


# Recommended Settings in Options
*Screen Tab*

* "Size of the session": All monitors
* Check "Resize remote session to the local window"
* Uncheck "Full screen mode"

# Security Tab

* Authentication method
      * Check "Password" if you want to provide your science account password every time you connect
      * Check "Public key" if you want to use your EC SSH private key to login (without password). To do so, you will have to concatenate the public key of your EC account  (i.e. ~/.ssh/id_rsa.pub) to `~/.ssh/authorized_keys` on Science. Then enter your EC private key file name in the "Key" field (~/.ssh/id_rsa).



# Tips and Tricks

## Launch with Autologin

To launch Thinlinc with autologin (without the connection dialog), do the following:

1. Set your connection to use a public key (see above)
2. In your EC account, edit the file ~/.thinlinc/tlclient.conf

    * Set the AUTOLOGIN key to 1

        `AUTOLOGIN=1`

3.  Launch `tlclient`


# Use Multiple Configuration Files

It is also possible to have multiple config files to handle different connection scheme (like with or without AUTOLOGIN). Specify one config file with the `-C` option:

`tlclient -C ~/.thinlinc/thinlinc_autologin.conf`

# Printing

To use your printer when connected to gpsc-vis through ThinLinc, use the thinlocal printer on gpsc-vis. This should work as long as you are able to print from the machine running `tlclient, be it a Windows or Linux machine`.

To print to EC network printers in Dorval, define the `PRINTER` environment variable on your EC workstation to select the printer:

1. Check for a default printer setting on EC side:

    `echo $PRINTER`

2. If `PRINTER` is empty

    * Get list of available printers:

        `lpstat -a`

    * Set `PRINTER` (e.g., in `~/.profile`):

        `if [ -z "${PRINTER}" ]; then`
           ` export PRINTER=<printername>`
        `fi`

3. Run `tlclient`.

# Common Issues

* Disable the application "Klipper" in the client's KDE session (the scissors tray icon). Having klipper active in the client will prevent you from using the clipboard (both with ctrl+c and mouse selection) of the host.  Once disabled, you can use the host's klipper for both the host and the client.
* You will find that any configuration that you wrote in $HOME/.profile.d/interactive is ignored in ThinLinc. That is because Ordenv is run when you do not have a terminal. To resolve this problem, look at the last section of the ordenv 4 User Guide.
* In the client, if you are unable to open X applications through konsole and get an error like "Can't open display :3", you are probably using a special recipe with the env command in your Konsole shell profile.  For this to work with X applications, you must provide the XAUTHORITY environment variable to your env command, so that it looks like this:

  `env -i USER=$USER XAUTHORITY=$XAUTHORITY DISPLAY=$DISPLAY LANG=$LANG LOGNAME=$LOGNAME TERM=xterm HOME=$HOME /bin/bash --login`

* If emacs crashes when you attempt to type an accented character with a dead key, do the following :

     * define the environment variable XMODIFIERS in the following way (e.g. in ~/.profile):

   ` export XMODIFIERS="@im=ibus"`

     * add the following line in ~/.emacs.d/init.el:

    `(require 'iso-transl)`

  * run emacs again.

* If your keymap is such that you need to type a dead key to get a c cedilla, you may not get it in emacs even after following the above recipe. You will have to do one of the following:

  * change the keymap layout on your machine
  * redefine yourself the dead key for the cedilla and show us your solution
  * or use xemacs21 instead of emacs.

* If you run graphical software that can take advantage of software acceleration or if you get OpenGL errors at execution time, use vglrun to start you application:

`vglrun program`

* If you get a segmentation fault with GTK error messages when running a graphical program (for example meld) inside ThinLinc, change the gtk2 theme from "oxygen-gtk" to "Raleigh" (click first on "System Settings" in the KDE menu, then on "Application Appearance" and GTK).
