---
altLangPrefix: gpsc1
dateModified: 2020-01-08
description:
  en: gpsc1
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: gpsc1
---


# La description

Gpsc1 (General Purpose Science Cluster) est un système de calcul Linux haute performance mis à disposition pour répondre aux besoins de calcul scientifique de divers ministères.

Le gpsc1 est composé de nœuds principaux, de nœuds de calcul, de gros nœuds et de nœuds GPU. Il n'y a pas d'accès interactif aux nœuds principaux et de calcul. Gpsc1 implémente des conteneurs pour fournir une isolation et s'adapter à différents environnements utilisateur. Les tâches utilisateur sont soumises à partir de gpsc-in et exécutées par lots dans des conteneurs dans un système d'exploitation disponible choisi (Ubuntu, CentOS, openSUSE, RedHat) sur les nœuds de calcul. Chaque conteneur de jobs batch est accessible de manière interactive. Le GPSC utilise un système de traitement par lots pour répartir la charge de travail dans les conteneurs du cluster.


# Spécification

|                      	| Head Nodes                         	| Compute Nodes                      	| Large Compute Nodes                   	| Super Large Compute Nodes             	| GPU (v100) Compute Nodes                      	|
|----------------------	|------------------------------------	|------------------------------------	|---------------------------------------	|---------------------------------------	|-----------------------------------------------	|
| Model                	| 2U Dell PowerEdge R720 rack server 	| Dell  PowerEdge M620 blade server  	| 2U Dell  PowerEdge  R820 rack  server 	| 2U Dell  PowerEdge  R820 rack  server 	|                                               	|
| # Nodes              	| 3                                  	| 256                                	| 10                                    	| 9                                     	| 3                                             	|
| CPUs (per node)      	| 2 x Intel Xeon E5-2650v2 @ 2.60GHz 	| 2 x Intel Xeon E5-2650v2 @ 2.60GHz 	| 4 x Intel Xeon E5-4650v2 @ 2.40GHz    	| 4 x Intel Xeon E7-8860v3 @ 2.20GHz    	| Intel(R)  Xeon(R)  Gold  6140 CPU  @  2.30GHz 	|
| Cores per node       	| 16                                 	| 16                                 	| 40                                    	| 64                                    	| 36                                            	|
| CPU threads per node 	| 16                                 	| 32                                 	| 80                                    	| 128                                   	| 72                                            	|
| Memory per node      	| 384 GB                             	| 128 GB                             	| 1 TB                                  	| 3 TB                                  	| 1024                                          	|
| Total Cores          	| 48                                 	| 4096                               	| 400                                   	| 576                                   	| 108                                           	|
| GPUs per Node        	|                                    	|                                    	|                                       	|                                       	| 4                                             	|
| Total GPUs           	|                                    	|                                    	|                                       	|                                       	| 12                                            	|
| Network              	| InfiniBand FDR                     	| InfiniBand FDR                     	| InfiniBand FDR                        	| InfiniBand FDR                        	| InfiniBand FDR                                	|
| Base OS              	| Ubuntu 18.04                       	| Ubuntu 18.04                       	| Ubuntu 18.04                          	| Ubuntu 18.04                          	| Ubuntu 18.04                                  	|
| Batch System         	| Gridengine                         	| Gridengine                         	| Gridengine                            	| Gridengine                            	| Gridengine                                    	|


Remarques:

* pour les nœuds de calcul, l'hyperthreading représente deux threads CPU par cœur, mais un seul thread CPU par cœur est alloué par le système de file d'attente.
* la mémoire disponible est toujours inférieure et dépendante du système de file d'attente
* [http://ark.intel.com/products/75269/Intel-Xeon-Processor-E5-2650-v2-20M-Cache-2_60-GH](http://ark.intel.com/products/75269 /Intel-Xeon-Processor-E5-2650-v2-20M-Cache-2_60-GH)
* [https://en.wikipedia.org/wiki/InfiniBand](https://en.wikipedia.org/wiki/InfiniBand)

# Système de file d'attente
## Ressources

| PE       	| Queue                         	| # Nodes 	| # Cpus   	| Memory 	|   	| Gpu Type 	|
|----------	|-------------------------------	|---------	|----------	|--------	|---	|----------	|
| dev      	| dev-be, dev-be-6h             	| 240     	| 100000M  	|        	|   	|          	|
| dev-In   	| dev-ln, dev-ln-6h             	| 10      	| 980000M  	|        	|   	|          	|
| dev-sln  	| dev-sln, dev-sln-6h           	| 9       	| 2800000M 	|        	|   	|          	|
| gpu-v100 	| dev-gpu-v100, dev-gpu-v100-6h 	| 3       	| 980000M  	| 4      	|   	| v100     	|

Remarques:

* les tâches doivent être soumises aux PE (`-pe <pename> <nslots>`), pas aux files d'attente
* les ressources disponibles peuvent ne pas correspondre aux ressources matérielles
* sélectionnez le type de GPU en utilisant `res_gputype` (par exemple, `-l res_gputype=v100`)

# Limites

| Type              	| Limit     	|
|-------------------	|-----------	|
| slots             	| 200/user  	|
| cpus (`res_cpus`) 	| 2048/user 	|



