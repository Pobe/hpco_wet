---
altLangPrefix: mise en mirroir
dateModified: 2020-01-08
description:
  en: mirroring tool
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Outil de mise en mirroir
---

# Introduction

Depuis la page d'accueil `rmirr` :

`rmirr` fournit un moyen *sûr* et facile à utiliser pour configurer et exécuter des instructions de mise en miroir de stockage de fichiers. Et parce qu'il utilise `rsync` pour effectuer la synchronisation, vous obtenez tous les avantages de performance et de disponibilité de `rsync`.


# Charger

`. ssmuse-sh -x main/opt/rmirr/rmirr-0.5`

Pour être utilisé, `rmirr` nécessite un fichier de configuration dans `~/.rmirr/rmirr.json`. Voir la documentation pour plus de détails.


# Référence

* [page d'accueil rmirr](https://expl.info/bin/view/Projects/RMIRR)
