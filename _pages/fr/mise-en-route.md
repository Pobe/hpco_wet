---
altLangPrefix: getting-started
dateModified: 2020-01-08
description:
  fr: mise en route
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Mise en route
---

> ###### **Contents**
> 1. [Obtenir un compte](#ObtenirUnCompte)
> 2. [Se connecter au compte](#SeConnecter)
> 3. [Configuration](#Configuration)
>     - [mettre en place ssh](#ssh)
>     - [mettre en place de l'environnement](#environnement)
> 4. [Aide](#Aide)


## Obtenir un compte
{: #ObtenirUnCompte}

Un compte personnel est requis pour tous les utilisateurs des ressources HPC.


*   [Comptes d'utilisateur](./comptes-utilisateur.html)


## Se connecter au compte
{: #SeConnecter}

Un conteneur interactif est en attente de votre connexion. Ceci est possible à partir de machines Linux/UNIX et Windows.


*   [Interactive Access](./interacitve-access.html)

## Configuration
{: #Configuration}

Une configuration initiale aurait dû être effectuée lors de la création du compte pour permettre à l'utilisateur de se connecter avec un mot de passe. 

Une configuration supplémentaire est requise par l'utilisateur pour utiliser les ressources HPC et obtenir de l'aide. Cette configuration doit être maintenue pour obtenir le meilleur service.

#### Mettre en place SSH
{: #ssh}

Une configuration minimale de SSH est requise.

*   [Mettre en place SSH](./mettre-en-place-ssh.html)

#### Mettre en place de l'environnement
{: #environnement}

Une configuration minimale de l'environnement de travail est requise.

*   [Mettre en place de l'environnement](./mettre-en-place-environnement.html)

## Aide
{: #Aide}

Si vous avez des problèmes avec les étapes ci-dessus, veuillez consulter:

*   [Obtenir de l'aide](./obtenir-aide.html)


[Retour](../index.html)