---
altLangPrefix: setting-up-the-environment
dateModified: 2020-01-08
description:
  fr: mettre en place de l'environnement
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Mettre en place de l'environnement
---

> ###### **Contenu**
> 1. [Introduction](#MettreEnPlaceEnvironnement-introduction)
> 2. [Shells supportés](#MettreEnPlaceEnvironnement-shellsSupportes)
> 3. [Dernière configuration](#MettreEnPlaceEnvironnement-derniereConfiguration)
> 4. [.profile contre .bashrc](#MettreEnPlaceEnvironnement-pcb)
> 5. [Voir aussi](#MettreEnPlaceEnvironnement-voirAussi)

# Introduction
{: #MettreEnPlaceEnvironnement-introduction}
La méthode standard pour configurer l'environnement est réalisée à l'aide du paquet `ordenv`. `ordenv` permet aux administrateurs de site, aux représentants des utilisateurs (communauté et groupe) et aux utilisateurs de configurer l'environnement.

> Une configuration par défaut devrait déjà avoir été effectuée lors de la création de votre compte.

# Shells supportés
{: #MettreEnPlaceEnvironnement-shellsSupportes}
Bien que d'autres Shells puissent être utilisées, le support est fourni pour :

* bash
* ksh

# Dernière configuration
{: #MettreEnPlaceEnvironnement-derniereConfiguration}
Assurez-vous que ce qui suit est dans votre `~/.profile` :
```
export ORDENV_SITE_PROFILE=20210129
export ORDENV_COMM_PROFILE=
export ORDENV_GROUP_PROFILE=
. /fs/ssm/main/env/ordenv-boot-20201118.sh
```
Notes:

* certains paquet standards sont chargés : `ssmuse`, `ssm`, `ordenv`
* le profil spécifique au site est chargé selon le paramètre dans `ORDENV_SITE_PROFILE`; ceci est applicable à **tous** les utilisateurs
* le profil spécifique à la communauté est chargé selon le paramètre de `ORDENV_COMM_PROFILE`; **consultez votre représentant d'utilisateur pour le modifier ou laissez vide**
* le profil spécifique au groupe est chargé selon le paramètre de `ORDENV_GROUP_PROFILE`; **consultez votre utilisateur ou représentant de groupe pour le modifier ou laissez vide**
* enfin, l'environnement spécifique à l'utilisateur est configuré en fonction du contenu de `~/.profile.d` ; consultez le [guide de l'utilisateur d'ordenv 4](https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide) pour savoir comment configurer

> Si votre environnement ne semble pas être configuré comme prévu, vérifiez que `ORDENV_COMM_PROFILE` et `ORDENV_GROUP_PROFILE` sont correctement définis.

# .profile contre .bashrc
{: #MettreEnPlaceEnvironnement-pcb}
Il est presque toujours préférable d'utiliser `~/.profile` pour configurer votre environnement Shell. Il est chargé automatiquement **pour les Shells de connexion, mais pas autrement**. Cela signifie que les appels tels que `ssh <host> date` s'exécutent avec un environnement minimal et, par conséquent, démarrent presque immédiatement. Des configuration supplémentaires pour le Shell de connexion peut être chargé dans la session courante en utilisant la commande `. /etc/profile` and/or `. ~/.profile` si nécessaire.    


`~/.bashrc` est utile pour les paramètres qui doivent être appliqués **chaque fois qu'un nouveau Shell est démarré**, y compris pour les Shells sans connexion. Par conséquence, il doit être utilisé judicieusement et ne doit généralement rien afficher sur la sortie standard.


# Voir aussi
{: #MettreEnPlaceEnvironnement-voirEgalement}
[Guide de l'utilisateur d'ordenv 4](https://portal.science.gc.ca/confluence/display/SCIDOCS/ordenv+4+User+Guide)
