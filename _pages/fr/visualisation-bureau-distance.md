---
altLangPrefix: visualisation-bureau-distance
dateModified: 2020-01-08
description:
  en: bureau à distance et visualisation
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Bureau à distance et visualisation
---

# Introduction

Thinlinc est un outil de bureau à distance utilisé par les utilisateurs en dehors du réseau scientifique pour accéder à un environnement de bureau Linux entièrement opérationnel sur le réseau scientifique à l'aide de leur poste de travail.


# Installation des clients

Vous devez installer le client ThinLinc sur votre ordinateur. Téléchargez-le https://www.cendio.com/thinlinc/download. Choisissez la version appropriée pour votre ordinateur.

> Pour les utilisateurs du réseau ECCC uniquement, le client ThinLinc est déjà installé et disponible via SSM en utilisant soit :

> `. ssmuse-sh -x main/opt/thinlinc/thinlinc_4.11.0`

> ou

> `. ssmuse-sh -d hpcs/ext/thinlinc-4.7.0`


# Installation du serveur

Vous n'avez pas besoin d'installer le serveur ThinLinc pour utiliser ThinLinc sur gpsc-vis ou hpcr-vis, où il est déjà installé. Pour ceux qui en ont besoin, voici les instructions d'installation :

1. Téléchargez le package .deb du serveur ThinLinc sur le site Web de Cendio (https://www.cendio.com/thilinc/download)

2. Installez sur un poste de travail avec la commande suivante (en tant que root) :

`dpkg -i $path/$package.deb`

3. Désactiver `firewalld`:

`sudo service firewalld stop`

4. Modifier `/etc/locale.gen` comme-ci:

`en_CA.UTF-8 UTF-8`
`en_US.UTF-8 UTF-8`
`fr_CA.UTF-8 UTF-8`

5. Redémarrer `firewalld`:

`sudo service firewalld start`


# Configuration et connexion du client

Pour démarrer le client, exécutez:

`tlclient`


# Lien

Serveur:

* `gpsc-vis.science.gc.ca` (tout le monde sauf la plupart des utilisateurs d'ECCC)
* `hpcr-vis.science.gc.ca` (la plupart des utilisateurs d'ECCC)


# Paramètres recommandés dans les options
*Onglet Écran*

* « Taille de la session » : tous les moniteurs
* Cochez "Redimensionner la session distante dans la fenêtre locale"
* Décochez "Mode plein écran"

# Onglet Sécurité

* Méthode d'authentification
      *Cochez « Mot de passe » si vous souhaitez fournir le mot de passe de votre compte scientifique à chaque fois que vous vous connectez
      * Cochez "Clé publique" si vous souhaitez utiliser votre clé privée EC SSH pour vous connecter (sans mot de passe). Pour ce faire, vous devrez concaténer la clé publique de votre compte EC (c'est-à-dire ~/.ssh/id_rsa.pub) dans `~/.ssh/authorized_keys` sur Science. Saisissez ensuite le nom de votre fichier de clé privée EC dans le champ "Clé" (~/.ssh/id_rsa).



# Trucs et astuces

## Lancement avec connexion automatique

Pour lancer Thinlinc avec la connexion automatique (sans la boîte de dialogue de connexion), procédez comme suit :

1. Configurez votre connexion pour utiliser une clé publique (voir ci-dessus)
2. Dans votre compte EC, modifiez le fichier ~/.thinlinc/tlclient.conf

    * Réglez la touche AUTOLOGIN sur 1

        `AUTOLOGIN=1`

3.  Démarrer `tlclient`


# Utiliser plusieurs fichiers de configuration

Il est également possible d'avoir plusieurs fichiers de configuration pour gérer différents schémas de connexion (comme avec ou sans AUTOLOGIN). Spécifiez un fichier de configuration avec l'option `-C` :

`tlclient -C ~/.thinlinc/thinlinc_autologin.conf`

# Impression

Pour utiliser votre imprimante lorsque vous êtes connecté à gpsc-vis via ThinLinc, utilisez l'imprimante thinlocal sur gpsc-vis. Cela devrait fonctionner tant que vous êtes capable d'imprimer à partir de la machine exécutant `tlclient, qu'il s'agisse d'une machine Windows ou Linux`.

Pour imprimer sur des imprimantes réseau EC à Dorval, définissez la variable d'environnement « IMPRIMANTE » sur votre poste de travail EC pour sélectionner l'imprimante :

1. Recherchez un paramètre d'imprimante par défaut du côté EC :

    `echo $PRINTER`

2. Si "IMPRIMANTE" est vide

    * Obtenir la liste des imprimantes disponibles :

        `lpstat -a`

    * Définir `PRINTER` (par exemple, dans `~/.profil`):

        `if [ -z "${PRINTER}" ]; then`
           ` export PRINTER=<printername>`
        `fi`

3. Démarrer `tlclient`.

# Problèmes courants

* Désactivez l'application "Klipper" dans la session KDE du client (l'icône de la barre des ciseaux). Avoir klipper actif dans le client vous empêchera d'utiliser le presse-papiers (à la fois avec ctrl+c et la sélection de la souris) de l'hôte. Une fois désactivé, vous pouvez utiliser le klipper de l'hôte pour l'hôte et le client.
* Vous constaterez que toute configuration que vous avez écrite dans $HOME/.profile.d/interactive est ignorée dans ThinLinc. C'est parce que Ordenv est exécuté lorsque vous n'avez pas de terminal. Pour résoudre ce problème, consultez la dernière section du Guide de l'utilisateur d'ordenv 4.
* Dans le client, si vous ne parvenez pas à ouvrir les applications X via konsole et obtenez une erreur du type "Can't open display :3", vous utilisez probablement une recette spéciale avec la commande env dans votre profil shell Konsole. Pour que cela fonctionne avec les applications X, vous devez fournir la variable d'environnement XAUTHORITY à votre commande env, afin qu'elle ressemble à ceci :

  `env -i USER=$USER XAUTHORITY=$XAUTHORITY DISPLAY=$DISPLAY LANG=$LANG LOGNAME=$LOGNAME TERM=xterm HOME=$HOME /bin/bash --login`

* Si emacs plante lorsque vous essayez de taper un caractère accentué avec une touche morte, procédez comme suit :

     * définir la variable d'environnement XMODIFIERS de la manière suivante (par exemple dans ~/.profile) :

   ` export XMODIFIERS="@im=ibus"`

     * ajoutez la ligne suivante dans ~/.emacs.d/init.el :

    `(nécessite 'iso-transl)`

  * relancer emacs.

* Si votre keymap est telle que vous devez taper une clé morte pour obtenir une cédille c, vous ne pouvez pas l'obtenir dans emacs même après avoir suivi la recette ci-dessus. Vous devrez effectuer l'une des opérations suivantes :

  * changer la disposition du clavier sur votre machine
  * redéfinissez vous-même la clé morte pour la cédille et montrez-nous votre solution
  * ou utilisez xemacs21 au lieu d'emacs.

* Si vous exécutez un logiciel graphique qui peut tirer parti de l'accélération logicielle ou si vous obtenez des erreurs OpenGL au moment de l'exécution, utilisez vglrun pour démarrer votre application :

`vglrun program`

* Si vous obtenez un défaut de segmentation avec les messages d'erreur GTK lors de l'exécution d'un programme graphique (par exemple meld) dans ThinLinc, changez le thème gtk2 de "oxygen-gtk" en "Raleigh" (cliquez d'abord sur "Paramètres système" dans le menu KDE, puis sur « Appearance de l'application » et GTK).