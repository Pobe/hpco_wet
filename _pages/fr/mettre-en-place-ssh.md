---
altLangPrefix: setting-up-ssh
dateModified: 2020-01-08
description:
  fr: Mettre en place SSH
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Mettre en place SSH
---

> ###### **Contenu**
> 1. [Introduction](#MettreEnPlaceSsh-introduction)
> 2. [clés ssh](#MettreEnPlaceSsh-clesSsh)
>     - [Procédure](#MettreEnPlaceSsh-procedure)
> 3. [Fichier de configuration SSH](#MettreEnPlaceSsh-fichierDeConfiguration)
>     - [Paramètres requis](#MettreEnPlaceSsh-parametresRequis)
> 4. [Autres configurations](#MettreEnPlaceSsh-autresConfigurations)
>     - [Éviter la déconnexion en cas d'inactivité](#MettreEnPlaceSsh-eviterLaDeconnexionInactivite)
>     - [Activation du *X11 Forwarding*](#MettreEnPlaceSsh-x11)

# Introduction
{: MettreEnPlaceSsh-introduction}

ssh est un outil indispensable qui est utilisé explicitement et en coulisse (par exemple, par le lanceur MPI).

Pour de nombreuses utilisations de ssh, il est nécessaire d'éviter toute interaction (par exemple, pour saisir un mot de passe, une phrase secrète ou un oui). Ceci est également vrai pour ce site.

# clés ssh
{: #MettreEnPlaceSsh-clesSsh}

ssh sans mot de passe est requis pour accéder aux services et soumettre des travaux. Cela se fait en utilisant des clés privées et publiques.

> **Clés RSA requises**    
> Les clés RSA sont désormais obligatoires. Ne vous attendez pas à ce que les clés DSA fonctionnent.

# Procédure
{: #MettreEnPlaceSsh-procedure}
1. Créez un répertoire `.ssh` dans le répertoire `$HOME` :
```
mkdir -p ~/.ssh
```

2. Modifiez la permission:
```
chmod -R 700 ~/.ssh
```

3. Changez de répertoire:
```
cd ~/.ssh
```

4. Générez une clé ssh:
```
ssh-keygen -q -t rsa
```
Remarque : _À moins que vous n'ayez des raisons spécifiques de ne pas le faire, laissez `ssh-keygen` utiliser le nom de fichier de clé suggérée (id_rsa)._

5. Ajoutez votre clé publique à votre fichier de clés autorisées.
Pour une connexion sans mot de passe en utilisant le même compte (ceci est requis pour accéder et soumettre des travaux sur le réseau de science HPC) :
```
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
```

6. (Facultatif) Pour une connexion sans mot de passe entre différents comptes d'utilisateurs, vous devez d'abord copier la clé publique du compte de source à celui de destination:
```
scp ~/.ssh/id_rsa.pub <destination username>@<hostname>:~
ssh <destination username>@<hostname>
cat ~/id_rsa.pub >> ~/.ssh/authorized_keys
rm -rf ~/id_rsa.pub
```
    
# Fichier de configuration ssh
{: #MettreEnPlaceSsh-fichierDeConfiguration}

## Paramètres requis
{: #MettreEnPlaceSsh-parametresRequis}

Ajoutez à `~/.ssh/config`:
```
Host *.science.gc.ca
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
Host *
   StrictHostKeyChecking no
   UserKnownHostsFile /dev/null
```
# Autres configurations
{: #MettreEnPlaceSsh-autresConfigurations}

## Éviter la déconnexion en cas d'inactivité
{: #MettreEnPlaceSsh-eviterLaDeconnexionInactivite}

Lors de la connexion au réseau de Science, une connexion inactive peut être interrompue pour diverses raisons. Le problème peut survenir à **tout moment** de la connexion.

Si vous rencontrez cela, essayez d'ajouter à `~/.ssh/config` du côté *client*:
```
avoid dropped idle connections
ServerAliveInterval 60
```
## Activation du *X11 Forwarding*
{: #MettreEnPlaceSsh-x11}

Pour activer du *X11 Forwarding*, ajoutez à `~/.ssh/config`:
```
ForwardX11 yes
```
Cela devrait être fait :

* pour votre compte HPC
* sur votre client de bureau
