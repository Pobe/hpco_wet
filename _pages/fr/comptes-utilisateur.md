---
altLangPrefix: user-accounts
dateModified: 2020-01-08
description:
  fr: Comptes d'utilisateur
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Comptes d'utilisateur
---

> ###### **Contents**
> 1. [Comptes d'utilisateur](#ComptesUtilisateur-compteUtilisateur)
>     - [Comptes personnels](#ComptesUtilisateur-comptesPersonnels)
>     - [Comptes partagés](#ComptesUtilisateur-comptesPartages)
>     - [Appartenance à un group](#ComptesUtilisateur-appartenanceGroup)
>     - [Stockage](#ComptesUtilisateur-stockage)
>     - [Mots de passe](#ComptesUtilisateur-mdps)
> 2. [Demandes de compte](#ComptesUtilisateur-demandeDeCompte)
>     - [Demander un compte d'utilisateur](#ComptesUtilisateur-demandeUnCompteUtilisateur)
>        - [Compte d'utilisateur partenaire](#ComptesUtilisateur-compteUtilisateurPartenaire)
>        - [Compte d'utilisateur collaborateur](#ComptesUtilisateur-compteUtilisateurCollaborateur)
>     - [Modifier les informations du compte](#ComptesUtilisateur-modifierLesInformations)
>     - [Mots de passe et réinitialisation de mot de passe](#ComptesUtilisateur-mdpsEtReinitialisation)
>     - [Fermer un compte d'utilisateur](#ComptesUtilisateur-fermerUnCompte)
>     - [Où soumettre](#ComptesUtilisateur-ouSoumettre)
>     - [Formulaires](#ComptesUtilisateur-formulaires)
> 3. [Voir aussi](#ComptesUtilisateur-voirAussi)


# Comptes d'utilisateur
{: #ComptesUtilisateur-compteUtilisateur}
Tous les utilisateurs **doivent** avoir un compte personnel. Les comptes génériques **ne sont pas** autorisés.

### Comptes personnels
{: #ComptesUtilisateur-comptesPersonnels}
Un compte personnel est créé pour un utilisateur individuel, et uniquement pour cet utilisateur. Le compte est lié à l'individu et **non** au groupe qui a demandé le compte. Si un utilisateur change de groupe, une mise à jour des informations de compte est **requise** par le groupe d'origine et le nouveau groupe.


### Comptes partagés
{: #ComptesUtilisateur-comptesPartages}
Un compte partagé est utilisé lorsque plusieurs utilisateurs doivent accéder à des ressources ou effectuer des opérations sous un compte non personnel. Les comptes partagés ne doivent jamais être utilisés comme comptes génériques ou comme substituts d'un compte personnel. Les comptes partagés appartiennent au groupe.


### Appartenance à un groupe
{: #ComptesUtilisateur-appartenanceGroup}
Un groupe permet de suivre l'utilisation des ressources et de contrôler les accès (par exemple, les permissions des répertoires) appliqués à un ou plusieurs utilisateurs.

Un compte utilisateur doit être membre d'au moins un groupe ; l'appartenance à plus d'un groupe est possible. L'appartenance au groupe est définie lors de la création du compte et modifiée lorsque l'utilisateur change de groupe.


### Stockage
{: #ComptesUtilisateur-stockage}
Le stockage *home* pour les comptes personnels est lié à l'utilisateur. Lorsqu'un utilisateur change de groupe, le contenu de *home* de l'utilisateur peut devoir être déplacé vers un autre emplacement de stockage appartenant au nouveau groupe.

Tout autre stockage disponible pour l'utilisateur est lié au groupe "propriétaire" du stockage.


### Mots de passe
{: #ComptesUtilisateur-mdps}
Les utilisateurs sont responsables de sécuriser leurs mots de passe à tout moment et de les modifier périodiquement. Les mots de passe sont sujets à expiration.


# Demandes de compte
{: #ComptesUtilisateur-demandeDeCompte}
### Demander un compte d'utilisateur
{: #ComptesUtilisateur-demandeUnCompteUtilisateur}
Les comptes d'utilisateur sont créés pour les partenaires (ministère, agence, ministère du gouvernement du Canada) **qui sont responsables de leurs utilisateurs et collaborateurs**. Tous les problèmes qui surviennent seront soulevés avec le demandeur/utilisateur, le gestionnaire et/ou le signataire/représentant autorisé.

> Les comptes sur science.gc.ca et collab.science.gc.ca sont distincts et une autorisation est requise pour chacun. Avoir un compte sur l'un des réseaux ne garantit pas que l'un ait un compte sur l'autre.


#### **Compte d'utilisateur partenaire**
{: #ComptesUtilisateur-compteUtilisateurPartenaire}
* Les demandes de compte **doivent** fournir toutes les informations demandées. S'il manque quelque chose, la demande sera rejetée/retournée.
* Les demandes de compte **doivent** être signées par un signataire autorisé : une personne inscrite auprès de HPCO et autorisée à allouer des ressources (appartenant à un groupe ou à un projet).
* Le formulaire de demande de compte doit être soumis avec la case "Nouveau" cochée.


#### **Compte d'utilisateur collaborateur**
{: #ComptesUtilisateur-compteUtilisateurCollaborateur}
Suivez les mêmes étapes que celles décrites dans [Compte d'utilisateur partenaires](#ComptesUtilisateur-compteUtilisateurPartenaire) avec les modifications/clarifications suivantes :

* Champs d'informations sur le candidat - les informations sur l'utilisateur du collaborateur.
* Champ Département - le partenaire qui parraine le collaborateur.
* Champ Nom du responsable - le responsable partenaire qui parraine la demande de compte du collaborateur et le contact pour traiter les problèmes des collaborateurs.


# Modifier les informations du compte
{: #ComptesUtilisateur-modifierLesInformations}
Une mise à jour du compte doit être effectuée lorsque :

* ajouter un accès à un nouveau système (par exemple, ajouter un accès à gpscc)
* l'utilisateur change de groupe
* la situation de sécurité de l'utilisateur change (par exemple, expire)

Le formulaire de demande de compte doit être soumis avec la case Modifier/Mettre à jour cochée.

### Mots de passe et réinitialisation de mot de passe
{:#ComptesUtilisateur-mdpsEtReinitialisation }
Une fois le compte créé, le demandeur recevra un courriel à l'adresse indiquée sur le formulaire de demande de compte. Le courriel sera encrypté à l'aide d'Entrust.

Pour demander une réinitialisation de mot de passe, envoyez une demande de service en utilisant le processus normal. Le nouveau mot de passe sera fourni de la même manière que ci-dessus.


### Fermer un compte d'utilisateur
{: #ComptesUtilisateur-fermerUnCompte}
Les comptes d'utilisateur **ne sont pas** supprimés.

Lorsqu'un compte d'utilisateur est fermé, l'accès à celui-ci est désactivé et l'accès au *home* peut être interdit (accès désactivé).

Le formulaire de demande de compte doit être soumis avec la case Fermer cochée.


### Où soumettre
{: #ComptesUtilisateur-ouSoumettre}
Voir [Obtenir de l'aide](./obtenir-aide.html) pour le processus de soumission du formulaire de demande de compte.


### Formulaires
{: #ComptesUtilisateur-formulaires}
[EScienceAccountFormRequest-FormulaireDemandeCompteEScience.pdf](https://portal.science.gc.ca/confluence/download/attachments/39289394/EScienceAccountRequest-DemandeCompteEScience.pdf?version=1&modificationDate=1621530506141&api=v2)


### Voir aussi
{: #ComptesUtilisateur-voirAussi}
[Obtenir de l'aide](./obtenir-aide.html)

