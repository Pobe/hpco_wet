---
altLangPrefix: démarrage-rapide-linux-cluster
dateModified: 2020-01-08
description:
  en:  Démarrage rapide de l'utilisation des clusters Linux avec SLURM
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title:  Démarrage rapide de l'utilisation des clusters Linux avec SLURM
---

# Introduction

Ce didacticiel explique comment utiliser les clusters Linux GPSC disponibles sur le réseau scientifique à l'aide des commandes SLURM natives. Les sujets incluent la compilation, l'exécution et la surveillance des programmes utilisateur.

Il suppose :

* vous avez déjà créé votre compte scientifique (voir Obtenir un compte HPC)
* vous avez déjà configuré votre environnement (voir Configurer votre environnement)
* vous avez une session interactive (voir Accès Interactif)

# Conditions

Le package gcc système par défaut doit être utilisé.

Le package openmpi suivant doit être utilisé :

openmpi-4.1.1 hpcx-2.8.0

# Temps de compilation

Pour compiler un programme MPI avec gcc, chargez uniquement la bibliothèque MPI suivante :
`. ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

> Si vous prévoyez d'utiliser le compilateur Intel avec MPI, veuillez d'abord en parler à votre représentant HPCO.

Lorsque vous utilisez des compilateurs Intel, chargez le bon compilateur :

`. ssmuse-sh -x main/opt/intelcomp/intelpsxe-cluster-19.0.3.199`

`. ssmuse-sh -x main/opt/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199`

# Durée

Au moment de l'exécution, chargez les éléments suivants :

`. ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`


Dans les exemples ci-dessous, une valeur d'image est utilisée pour la démonstration. Assurez-vous d'utiliser la valeur qui s'applique à vous. Sinon, votre tâche ne s'exécutera pas.

Les images de conteneurs disponibles se trouvent sous les pages spécifiques aux partenaires. Commencer à:

[Informations sur les partenaires](https://portal.science.gc.ca/confluence/display/SCIDOCS/Partner+Information)

et sélectionnez le lien qui vous concerne. Ensuite, suivez les liens vers gpsc → images de conteneur gpsc. Vous pouvez ensuite utiliser/essayer l'une des valeurs répertoriées.


# Exemples

Cette section fournit des exemples pour :

* programme à thread unique
* Programme MPI - multi-nœuds, simple thread
* Programme OpenMP - nœud unique, multi-thread, mémoire partagée
* OpenMP et MPI combinés

Les exemples seront avec les commandes sbatch et srun.

La principale différence est que *srun* est interactif *et* bloquant (vous obtenez le résultat dans votre terminal *et* vous ne pouvez pas écrire d'autres commandes tant qu'il n'est pas terminé), tandis que *sbatch* est un traitement par lots *et* non bloquant ( les résultats sont écrits dans un fichier *et* vous pouvez soumettre d'autres commandes immédiatement).


# Installer

Créez le répertoire de travail :

`$ mkdir -p ~/tmp/démarrage rapide`

> Lors de la création de vos fichiers batch ou de l'exécution de vos commandes srun, assurez-vous d'inclure les directives --account et --time qui définissent le projet et l'horloge murale. Ces deux directives sont obligatoires.

# Programme à thread unique

Cet exemple montre comment créer et soumettre un travail pour un programme à thread unique.

Créer un nouveau fichier `hello.c`:

    /*

    ** hello.c

    */

    #include <stdio.h>

    int

    main(int argc, char **argv) {

    printf("hello\n");}

Compiler avec gcc:

`$ gcc -o hello hello.c`


Exécutez le programme de manière interactive :


`$ ./hello`


Créer un fichier de travail `hello.sh`:

    #!/bin/bash -l

    #SBATCH --job-name=hello

    #SBATCH --output=hello.out

    #SBATCH --partition=standard

    #SBATCH --account=nrc_aero

    #SBATCH --time=00:00:60

    #SBATCH --ntasks=1

    #SBATCH --cpus-per-task=1

    #SBATCH --mem-per-cpu=2000M

    #SBATCH --comment="nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    cd ~/tmp/quickstart

    ./hello

| Directive                                                         	| Explanation                                                                                                                                                                                     	|
|-------------------------------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| #SBATCH --account=nrc_aero                                        	| Specify the project for accounting purposes.  To know which project you can submit to check,  gpsc - Gridengine - Projects within your  ministry.                                               	|
| #SBATCH --comment="nrc/nrc_all_default_ubuntu-18.04-amd64_latest" 	| The program will run inside this kind of  container. *Make sure it matches the partner  you belong to and the distribution you want*.                                                           	|
| #SBATCH --cpus-per-task=1                                         	| 1 cpu is required.                                                                                                                                                                              	|
| #SBATCH --job-name=hello                                          	| Modify job name as needed.                                                                                                                                                                      	|
| #SBATCH --mem-per-cpu=2000M                                       	| 2000MB is requested. This is a  reasonable minimum.                                                                                                                                             	|
| #SBATCH --ntasks=1                                                	| This option advises the Slurm controller  that job steps run whithin the allocation  will launch with a maximun number of 1 task  and to provide the sufficient resources. The  default is one. 	|
| #SBATCH --output=hello.out                                        	| Job output is sent to /hello.out                                                                                                                                                                	|
| #SBATCH --partition=standard                                      	| standard queue is chosen                                                                                                                                                                        	|
| #SBATCH --time=00:00:60                                           	| 60s of wallclock/run time to complete.                                                                                                                                                          	|

pour exécuter le fichier batch :

`$ sbatch hello.sh`


Pour lancer une tâche sur un cluster spécifique :

`$ sbatch --cluster=<clustername> hello.sh`


Pour exécuter la tâche de manière interactive :

`$ srun --account nrc_aero --partition standard --ntasks 1 --cpus-per-task 1 --mem-per-cpu 2000M --comment "nrc/nrc_all_default_ubuntu-18.04-amd64_latest" --time 00:00:60 --mpi none ./hello`


# MPI Hello World

Cet exemple montre comment créer et soumettre un travail pour un programme à plusieurs nœuds à l'aide de MPI.

Créer un nouveau fichier `mpihello.c` :

    /*
    ** mpihello.c
    */

    #include <mpi.h>
    #include <stdio.h>
    #include <heure.h>

    int main(int argc, char** argv) {

    // Initialiser MPI.
    MPI_Init(NULL, NULL);

    // Récupère le nombre de processus
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Récupère le rang du processus
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Récupère le nom du processeur
    char processor_name[MPI_MAX_PROCESSOR_NAME] ;
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Veille pendant un nombre aléatoire de secondes. (1 à 5).
    // Utilise la version thread_safe de rand().
    int temps de sommeil;
    srand(heure(NULL));
    heure de sommeil = (rand_r() % 5) + 1 ;
    dormir (l'heure du sommeil);

    // Récupère l'heure actuelle, avec une précision de la milliseconde.
    struct timespec mytime;
    if (gettimeofday(&mytime, NULL) == 0)
    // Identité du processus de sortie et temps de veille.
    printf("Bonjour du processeur %s, rang %d sur %d processeurs ! J'ai dormi %d secondes jusqu'à %d nanosecondes après %s",
    nom_processeur, rang_monde, taille_monde, heure de veille, monheure.tv_nsec, ctime(&mytime.tv_sec));
    autre
    // Message d'erreur de sortie.
    printf("Bonjour du processeur %s, rang %d sur %d processeurs ! Désolé, je n'ai pas pu accéder à l'horloge.\n",
    nom_processeur, rang_monde, taille_monde);

    // Finalisation de l'environnement MPI. Plus d'appels MPI autorisés.
    MPI_Finaliser();

    retour(0);
    }

Pour construire un programme MPI, des wrappers de compilateur sont utilisés :

Chargez d'abord le package openmpi :

`$ . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

`mpicc` appelle le compilateur sous-jacent (`gcc` dans ce cas) avec les fichiers d'inclusion et de bibliothèque MPI requis.

`$ mpicc -o mpihello mpihello.c`

Créer un fichier de travail `mpihello.sh`

    #!/bin/bash -l
    #SBATCH --export=USER,LOGNAME,HOME,MAIL,PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    #SBATCH --job-name=mpihello
    #SBATCH --output=mpihello.out
    #SBATCH --account=nrc_aero
    #SBATCH --partition=standard
    #SBATCH --time=00:10:00
    #SBATCH --nodes=2
    #SBATCH --ntasks=8
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export SLURM_EXPORT_ENV=ALL

    # load mpi environment
    . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc
    . ssmuse-sh -x main/opt/openmpi-setup/openmpi-setup-0.3-hcoll

    cd ~/tmp/quickstart
    mpirun ./mpihello


Directives de travail expliquées :

| Directive                              	| Explanation                                                                                                                                                                                 	|
|----------------------------------------	|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| #SBATCH --time=00:10:00                	| 10 minutes to complete.                                                                                                                                                                     	|
| #SBATCH --ntasks=8                     	| This option advises the SLURM controller that job steps run within the allocation will launch with a maximum number of 8 tasks and to provide the sufficient resources. The default is one. 	|
| #SBATCH --cpus-per-task=8              	| 8 cpus per slot. This gives a total of 8x8=64 cpus to run the job                                                                                                                           	|
| #SBATCH --mem-per-cpu=4000M            	| 4000MB per slot; this gives a total of 2x4000=8000 MB to run the job                                                                                                                        	|
| #SBATCH --nodes=2                      	| 2 nodes are required                                                                                                                                                                        	|
| #SBATCH --export=USER,LOGNAME,HOME ... 	| changes the SLURM_EXPORTENV environment variable. Doing export SLURM_EXPORT_ENV=ALL sets the variable to all when we launch the MPI program.                                                	|

pour exécuter le travail par lots :

`$ sbatch mpihello.sh`

# OpenMP Hello World

Cet exemple montre comment créer et soumettre un travail pour un programme multithread à l'aide d'OpenMP.

Créer un nouveau fichier `omphello.c`:

    /*
    *omphello.c
    */

    #include <omp.h>
    #include <stdio.h>
    #include <stdlib.h>

    int
    main(int argc, char **argv) {
    int tid;

    #pragma omp parallel private(tid)
    {
    tid = omp_get_thread_num();
    printf("Hello World from thread = %d\n", tid);
    if (tid == 0) {
    printf("Number of threads (%d)\n", omp_get_num_threads());
    printf("OMP_NUM_THREADS (%s)\n", getenv("OMP_NUM_THREADS"));
    }
    }
    exit(0);
    }

Pour créer un programme OpenMP, spécifiez l'option spécifique au compilateur requise :

`$ gcc -fopenmp -o omphello omphello.c`

Pour exécuter, définissez la variable d'environnement `OMP_NUM_THREADS` (bien que non obligatoire, `OMP_NUM_THREADS` <= nombre de processeurs disponibles) :

`$ export OMP_NUM_THREADS=4`
`$ ./omphello`

Créez le fichier de tâche `omphello.sh` (assurez-vous de définir `OMP_NUM_THREADS`):

    #!/bin/bash -l
    #SBATCH --job-name=omphello
    #SBATCH --output=omphello.out
    #SBATCH --partition=standard
    #SBATCH --account=nrc_aero
    #SBATCH --time=00:00:60
    #SBATCH --nodes=1
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export OMP_NUM_THREADS=4
    # run program
    cd ~/tmp/quickstart
    ./omphello

Soumettre le travail :

`$ sbatch omphello.sh`

Si `OMP_NUM_THREADS` n'est *pas* défini, le programme essaiera de déterminer le nombre de processeurs disponibles et utilisera autant de threads. Si une tâche s'exécute dans un conteneur, ne pas spécifier le "OMP_NUM_THREADS" fonctionnera mais plus de processeurs que prévu peuvent être utilisés. Il est généralement préférable de définir `OMP_NUM_THREADS`.

soumettre le travail de manière interactive :

`$ export OMP_NUM_THREADS=4`
`$ srun --account nrc_aero --partition standard --nodes 1 --cpus-per-task 8 --mem-per-cpu 4000M --comment "nrc/nrc_all_default_ubuntu-18.04-amd64_latest" --time 00:00:60 mpirun ./omphello`

# OpenMP+MPI Hello World

Ce code contient des parties de `mpihello.c` et `omphello.c`.

Créer un fichier `mpiomphello.c`:

    /*
    ** mpiomphello.c
    */

    #include <mpi.h>
    #include <omp.h>
    #include <stdio.h>
    #include <heure.h>

    int main(int argc, char** argv) {
    int iam = 0, np = 1;

    // Initialiser MPI.
    MPI_Init(NULL, NULL);

    // Récupère le nombre de processus
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Récupère le rang du processus
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Récupère le nom du processeur
    char processor_name[MPI_MAX_PROCESSOR_NAME] ;
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Veille pendant un nombre aléatoire de secondes. (1 à 5).
    // Utilise la version thread_safe de rand().
    int temps de sommeil;
    srand(heure(NULL));
    heure de sommeil = (rand_r() % 5) + 1 ;
    dormir (l'heure du sommeil);

    // Récupère l'heure actuelle, avec une précision de la milliseconde.
    struct timespec mytime;
    if (gettimeofday(&mytime, NULL) == 0)
    // Identité du processus de sortie et temps de veille.
    printf("Bonjour du processeur %s, rang %d sur %d processeurs ! J'ai dormi %d secondes jusqu'à %d nanosecondes après %s",
    nom_processeur, rang_monde, taille_monde, heure de veille, monheure.tv_nsec, ctime(&mytime.tv_sec));
    autre
    // Message d'erreur de sortie.
    printf("Bonjour du processeur %s, rang %d sur %d processeurs ! Désolé, je n'ai pas pu accéder à l'horloge.\n",
    nom_processeur, rang_monde, taille_monde);

    #pragma omp parallèle par défaut (partagé) privé (iam, np)
    {
    np = omp_get_num_threads();
    iam = omp_get_thread_num();
    printf("Bonjour du thread OMP %d sur %d du processus MPI %d sur %d sur %s\n",
    iam, np, world_rank, world_size, processor_name);
    }


    // Finalisation de l'environnement MPI. Plus d'appels MPI autorisés.
    MPI_Finaliser();

    retour(0);
    }

Compilez en utilisant le wrapper `mpcc` avec l'option requise pour OpenMP :

`$ . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc`

Remarque : l'option `gcc -fopenmp` est spécifique au compilateur.

`$ mpicc -fopenmp mpiomphello.c -o mpiomphello`


Créez le fichier de tâche `mpiomphello.sh` :

    #!/bin/bash -l
    #SBATCH --export=USER,LOGNAME,HOME,MAIL,PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    #SBATCH --job-name=mpiomphello
    #SBATCH --output=mpiomphello.out
    #SBATCH --partition=standard
    #SBATCH --account=nrc_aero
    #SBATCH --time=00:00:60
    #SBATCH --nodes=4
    #SBATCH --ntasks=4
    #SBATCH --cpus-per-task=8
    #SBATCH --mem-per-cpu=4000M
    #SBATCH --comment="image=nrc/nrc_all_default_ubuntu-18.04-amd64_latest"

    export SLURM_EXPORT_ENV=ALL

    # load mpi environment
    . ssmuse-sh -x main/opt/openmpi/openmpi-4.1.1a1--hpcx-2.8.0-mofed-5.1--gcc
    . ssmuse-sh -x main/opt/openmpi-setup/openmpi-setup-0.3-hcoll

    export OMP_NUM_THREADS=8
    cd ~/tmp/quickstart
    mpirun -n 4 ./ompmpihello


    #pour exécuter le script avec la commande srun
    srun ./ompmpihello


Soumettre le travail :

`$ sbatch mpiomphello.sh`

Soumettre le travail :# Surveillance des travaux

Il existe deux façons de surveiller vos travaux avec Slurm.

## file d'attente

En faisant :

`$ squeue -u <nom d'utilisateur>`

Une liste des travaux qui sont dans la file d'attente pour le nom d'utilisateur donné avec l'ID de travail, le nom, l'état et d'autres informations utiles sera affichée.

## sacct

sacct donne des informations sur les tâches qui ont été lancées sur un cluster donné à partir d'intervalles de temps spécifiques et peut donner des informations plus utiles en fonction des paramètres de commande.

Un exemple d'une telle commande est :

`$ sacct -a -p S 2020-01-11 E 2021-01-11 --format jobid,start,end,state,ReqNodes,Account,Partition,ncpus,ReqMem,group,JobName,user,timelimitRaw `

Cette commande répertorie les travaux qui ont été lancés entre 2020-01-11 et 2021-01-11 avec les paramètres répertoriés générés sous forme de colonnes.

Pour plus d'informations sur des champs de sortie spécifiques, consultez https://slurm.schedmd.com/sacct.html .

