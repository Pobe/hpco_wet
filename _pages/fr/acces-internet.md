---
altLangPrefix: internet-access
dateModified: 2020-01-08
description:
  en: Internet Access from the Science and Collab Science Networks
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Accès Internet à partir des réseaux Science et Collab Science
---

# Introduction

L'accès à Internet à partir du réseau science.gc.ca n'est pas uniforme :

- Conteneurs interactifs - peuvent accéder à Internet normalement.
- Les travaux exécutés sur les clusters - exécutés dans une partie du réseau scientifique qui a un accès limité à Internet.

> Les données de transfert sont recommandées.
Comme les clusters sont destinés au calcul haute performance, il est fortement recommandé que toutes les données requises soient disponibles localement sur le réseau scientifique plutôt que d'être extraites d'un travail. Il y a des moments où cette pratique peut être assouplie, mais toutes les opérations qui passent beaucoup de temps à attendre des services externes au réseau scientifique peuvent être inutilement coûteuses.

# Services Web

Un serveur proxy est disponible pour permettre l'accès au Web (http et https uniquement) à partir des tâches exécutées sur les nœuds du cluster

Pour science.gc.ca :

    exporter http_proxy=http://webproxy.science.gc.ca:8888/

    exporter https_proxy=http://webproxy.science.gc.ca:8888/

Pour collab.science.gc.ca :

    exporter http_proxy=http://webproxy.collab.science.gc.ca:8888/

    exporter https_proxy=http://webproxy.collab.science.gc.ca:8888/

Remarque : Le proxy n'est *pas* nécessaire lors de l'exécution dans des conteneurs interactifs.

# Accès FTP

Il n'y a actuellement aucune prise en charge pour accéder aux serveurs FTP à partir des tâches exécutées sur les clusters.