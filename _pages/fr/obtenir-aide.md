---
altLangPrefix: getting-support
dateModified: 2020-01-08
description:
  fr: obtenir-de-l'aide
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Obtenir de l'aide
---

> ###### **Contenu**
> 1. [Représentant du soutien](#ObtenirAide-representantDuSoutien)
> 2. [Processus](#ObtenirAide-processus)
> 3. [Listes des courriels](#ObtenirAide-listesDiffusion)
> 4. [Contacts](#ObtenirAide-contacts)

# Représentant du soutien
{: #ObtenirAide-representantDuSoutien}
Un représentant du soutien aux partenaires HPCO est assigné à chaque partenaire, qui est la principale ressource pour obtenir de l'aide. Le représentant du soutien fournit des informations, une formation, des conseils et est responsable de la gestion des problèmes et des demandes de service.


# Processus
{: #ObtenirAide-processus}
Le processus pour obtenir de l'aide est le suivant :

1. Demandez de l'aide à votre représentant local des utilisateurs, qui est votre première source de l'aide.
2. Soumettez votre problème/demande à votre bureau d'assistance local (qui à son tour le soumettra au bureau de service SSC). Envoyez une copie carbone (CC) à [ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca](mailto:ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca). Ajoutez le paragraphe suivant, quelque part dans votre demande:

> Veuillez assigner au groupe SSC/HPCO avec le groupe propriétaire DC000134.
> Veuillez vous assurer que toutes les pièces jointes sont copiées.

Pour les questions auxquelles on peut répondre **facilement** et **rapidement** :

1. Envoyez un courriel à votre représentant du soutien aux partenaires HPCO, ou
2. Envoyer un courriel à la boîte de réception du service HPCO ([hpcoptimizationservice-serviceoptimisationchp@ssc-spc.gc.ca](mailto:hpcoptimizationservice-serviceoptimisationchp@ssc-spc.gc.ca)).

# Listes de diffusion
{: #ObtenirAide-listesDiffusion}
Des listes de diffusion ont été créées pour les partenaires et les projets. Elles sont utilisées pour la communication entre le représentant de soutien, le partenaire et les utilisateurs du projets.    
Veuillez ne pas utiliser ces listes pour une assistance générale. Suivez plutôt le processus de la section ci-dessus.

# Contacts
{: #ObtenirAide-contacts}

| Partenaire/Projet  | Représentant                    | Backup          | listes de diffusion                                                         |
|--------------------|:--------------------------------|:----------------|:----------------------------------------------------------------------------|
| AAFC               | Bernie Genswein                 |                 | [Lien](mailto:hpco-aafc-partner-support-ochp-aac-soutien-au-partenaire@lists.ec.gc.ca)     |
| CFIA               | Bernie Genswein                 | John Marshal    |                                                                             |
| CSA                | Michel Béland                   |                 |                                                                             |
| DFO                | John Marshall / Bernie Genswein |                 | [Lien](mailto:hpco-dfo-partner-support-ochp-mpo-soutien-au-partenaire@lists.ec.gc.ca)      |
| ECCC (HPC-MAC)     | Michel Béland                   | Alain Desmeules | [Lien](mailto:hpc-user-reps@lists.ec.gc.ca)(broken at the moment)                         |
| ECCC (non HPC-MAC) | Alain Desmeules                 | Michel Béland   |                                                                             |
| GRDI               | Bernie Genswein                 |                 |                                                                             |
| MC                 |                                 |                 |                                                                             |
| NRC                | John Marshall / Moussa Abdenbi  | Michel Béland   | [Lien](mailto:hpco-nrc-partner-support-ochp-cnrc-soutien-au-partenaire@lists.ec.gc.ca)     |
| NRCAN              | Antoine LeBel                   | John Marshall   | [Lien](mailto:hpco-nrcan-partner-support--ochp-rncan-soutien-au-partenaire@lists.ec.gc.ca) |
| PHAC               | John Marshall                   |                 |                                                                             |
| StatCan            | Moussa Abdenbi                  |                 |                                                                             |
