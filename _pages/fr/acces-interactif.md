---
altLangPrefix: interactive access
dateModified: 2020-01-08
description:
  fr: interactive access
footer: true
langtoggle: true
lang: fr
noMainContainer: false
sitemenu: true
sitesearch: true
title: Accès Intéractif
---

> ###### **Contenu**
> 1. [Introduction](#AccesInteractif-introduction)
> 2. [ThinLinc](#AccesInteractif-thinlinc)
> 3. [ssh / PuTTY](#AccesInteractif-sshputty)
>     - [Point d'entrée / Hôtes de connexion](#AccesInteractif-pointEntreeHotesConnexion)
>     - [Sélection d'un conteneur interactif](#AccesInteractif-selectionConteneur)
>     - [Connexion à un conteneur interactif](#AccesInteractif-connexionConteneur)
>        - [Linux - ssh](#AccesInteractif-connexionConteneurLinuxSsh)
>        - [Windows - PuTTY](#AccesInteractif-connexionConteneurWindowsPutty)
>     - [Se connecter avec le *X11 Forwarding* activé](#AccesInteractif-connexionAvecX11)
>        - [Linux](#AccesInteractif-connexionAvecX11Linux)
>     - [Windows - PuTTY](#AccesInteractif-windowsPutty)

# Introduction
{: #AccesInteractif-introduction}
L'accès interactif au réseau **Science** est fourni en utilisant :

* ThinLinc
* ssh / PuTTY

L'accès interactif au réseau **Collab Science** est fourni en utilisant :

* ssh / PuTTY

> Les réseaux Science et Collab Science sont distincts, séparés et totalement indépendants. Bien que les noms de compte puissent être les mêmes sur les deux, ils **ne partagent pas** les paramètres de compte, les systèmes de fichiers personnels, le stockage. Aussi, le réseau Collab Science est accessible depuis le réseau Science, mais pas l'inverse.

# ThinLinc
{: #AccesInteractif-thinlinc}
ThinLinc fournit un environnement de bureau à distance. Pour plus d'informations voir :

[ThinLinc - Bureau à distance et visualisation](https://portal.science.gc.ca/confluence/display/SCIDOCS/ThinLinc+-+Remote+Desktop+and+Visualisation)

Voir la section Point d'entrée ci-dessous pour l'hôte auquel se connecter.

# ssh / PuTTY
{: #AccesInteractif-sshputty}
ssh / PuTTY fournissent une session ssh et terminal.

## Point d'entrée / Hôtes de connexion
{: #AccesInteractif-pointEntreeHotesConnexion}
Le point d'entrée du réseau Science est à :

* `sci-eccc-in.science.gc.ca` - utilisateurs du CMC seulement
* `gpsc-in-gccloud.science.gc.ca` - Externe/tous les autres utilisateurs
* `gpsc-in.science.gc.ca` - Alternative si `gpsc-in-gccloud.science.gc.ca` ne répond pas

Pour tous les exemples ci-dessous, `gpsc-in-gccloud.science.gc.ca` sera utilisé comme point d'entrée.

Le point d'entrée du réseau Collab Science est à :

*  `gpscc-in.collab.science.gc.ca` - Accessible depuis Internet.

## Sélection d'un conteneur interactif
{: #AccesInteractif-selectionConteneur}
L'environnement de chaque utilisateur est préconfiguré pour se connecter à un conteneur interactif qui a été spécifié sur le formulaire de création de compte.

Chaque partenaire dispose d'un ou plusieurs conteneurs interactifs dédiés à son usage. Par exemple.,

* `<partner>-interactive--centos-6.7-amd64-64`
* `<partner>-interactive--ubuntu-18.04-amd64-64`

Où <partner> est dfo, nrc, nrcan, etc. Par exemple, pour les utilisateurs du NRC, les conteneurs interactifs suivants sont disponibles :

* `nrc-interactive--centos-6.7-amd64-64`
* `nrc-interactive--ubuntu-18.04-amd64-64`

> **Accès restreint**    
> Les utilisateurs doivent utiliser leurs propres conteneurs spécifiques au partenaire.

Pour définir/modifier le paramètre du conteneur interactif (En utilisant `nrc-interactive--ubuntu-18.04-amd64-64` comme exemple):
```
echo "nrc-interactive--ubuntu-18.04-amd64-64" > ~/.sshj/interactive/gpsc1.science.gc.ca/jobname
```

Si le paramètre préconfiguré n'est pas correct ou si vous souhaitez le modifier, utilisez l'un des paramètres de votre partenaire qui vient d'être décrit ou contactez votre représentant local des utilisateurs ou contactez [ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca.](mailto :ssc.hpcoptimizationservice-serviceoptimisationchp.spc@canada.ca
)

## Connexion à un conteneur interactif
{: #AccesInteractif-connexionConteneur}
### Linux - ssh
{: #AccesInteractif-connexionConteneurLinuxSsh}
Dans un terminal, ssh vers un point d'entrée (`gpsc-in-gccloud.science.gc.ca`) et entrez votre nom d'utilisateur et votre mot de passe :
```
ssh username@gpsc-in-gccloud.science.gc.ca
```

### Windows - PuTTY
{: #AccesInteractif-connexionConteneurWindowsPutty}
1. Cliquez sur le menu Démarrer > Programmes > PuTTY
2. Dans PuTTY :
      * Sous Catégorie > Session > Nom d'hôte, entrez le nom d'hôte du point d'entrée (`gpsc-in-gccloud.science.gc.ca`), puis cliquez sur Ouvrir (Figure 1)
      * Entrez votre nom d'utilisateur et votre mot de passe

Figure 1:   
![Putty](../media/putty_gccloud.png){: .img-responsive}


## Se connecter avec le *X11 Forwarding* activé
{: #AccesInteractif-connexionAvecX11}
Le *X11 Forwarding* devrait déjà être activé par défaut lors de la création de votre compte. Veuillez vérifier votre `~/.ssh/config` pour vous assurer que c'est le cas.

### Linux
{: #AccesInteractif-connexionAvecX11Linux}
Dans un terminal (par exemple, en utilisant l'hôte du point d'entrée `gpsc-in-gccloud.science.gc.ca`) :
```
ssh username@gpsc-in-gccloud.science.gc.ca
```

Ajoutez la ligne suivante à votre fichier `~/.ssh/config`:
```
ForwardX11 yes
```

Démarrez une nouvelle session avec le *X11 Forwarding* activé :
```
ssh -X username@gpsc-in-gccloud.science.gc.ca
```

## Windows - PuTTY
{: #AccesInteractif-windowsPutty}
Connectez-vous à un point d'entrée :

1. Cliquez sur le menu Démarrer > Programmes > PuTTY
2. Dans PuTTY :
    * Sous Catégorie > Session > Nom d'hôte, entrez le nom d'hôte du point d'entrée (`gpsc-in-gccloud.science.gc.ca`), puis cliquez sur Ouvrir (Figure 1)
    * Entrez votre nom d'utilisateur et votre mot de passe

Ajoutez la ligne suivante à votre fichier `~/.ssh/config`:
```
ForwardX11 yes
```

> Pour utiliser le *X11 Forwarding* sous Windows, vous devez disposer d'un serveur X pour Windows (par exemple Xming, MobaXterm) exécuté sur votre poste de travail Windows local.

Utilisation de MobaXterm :

* Démarrer une nouvelle session avec le *X11 Forwarding* activé :
```
ssh -X username@gpsc-in-gccloud.science.gc.ca
```
Utilisation de Xming avec PuTTY :

1. Démarrer Xming
2. Dans PuTTY :
      * Sous Catégorie > Session > Connexion > SSH > Section "X11 Forwarding", cochez "Activer le X11 Forwarding" (Figure 2)
      * Sous Catégorie > Session > Nom d'hôte, entrez le nom d'hôte du point d'entrée (`gpsc-in-gccloud.science.gc.ca`), puis cliquez sur Ouvrir (Figure 1)
      * Entrez votre nom d'utilisateur et votre mot de passe

Figure 2: Enable X11 forwarding    
![Putty_x11](../media/putty_x11.png){: .img-responsive}
