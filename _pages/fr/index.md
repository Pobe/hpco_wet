---
altLangPrefix: index
authorName: Service Canada
authorUrl:
contentTitle: Canada.ca Jekyll theme
description:
  en: "Get quick, easy access to all Government of Canada services and information."
  fr: "Accédez rapidement et facilement à tous les services et renseignements du gouvernement du Canada."
lang: fr
#layout: page
layout: default
noContentTitle: true
pageclass: wb-prettify all-pre
subject:
  en: [GV Government and Politics, Government services]
  fr: [GV Gouvernement et vie politique, Services gouvernementaux]
title: Test page - Canada.ca
---
- [Mise en route](./mise-en-route.html).
- [Compte d'utilisateurs](./comptes-utilisateur.html).
- [Mettre en place SSH](./mettre-en-place-ssh.html).
- [Mettre en place de l'environnement](./mettre-en-place-environnement.html).
- [Obtenir de l'aide](./obtenir-aide.html)
- [Accès interactif](./acces-interactif.html)
- [Ressources de Calcul](./ressources-calcul.html)
- [GPSC1](./gpsc1.html)
- [Bureau à distance et visualisation](./visualisation-bureau-distance.html)
- [Démarrage rapide de l'utilisation des clusters Linux avec SLURM](./démarrage-rapide-linux-cluster.html)
- [Mise en mirroir](./mise-en-mirroir.html)
