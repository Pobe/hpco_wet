---
altLangPrefix: compute resources
dateModified: 2020-01-08
description:
  en: compute resources
footer: true
langtoggle: true
lang: en
noMainContainer: false
sitemenu: true
sitesearch: true
title: Ressources de Calcul
---

Il existe un certain nombre de systèmes HPC disponibles pour une utilisation par lots et interactive. Des systèmes pour une utilisation par lots sont disponibles pour exécuter des tâches en série ou des tâches parallèles (MPI et OpenMP). Des systèmes dédiés à une utilisation interactive sont disponibles pour exécuter des outils graphiques, pour compiler, déboguer, soumettre des travaux par lots et pour exécuter des travaux non intensifs en calcul sans planification.


| System   	| Network             	| Batch 	| Interactive 	| Batch System 	|
|----------	|---------------------	|-------	|-------------	|--------------	|
| gpsc-in  	| science             	|       	| X           	| N/A          	|
| gpsc-vis 	| science             	|       	| X           	| N/A          	|
| gpsc1    	| science             	| X     	|             	| Gridengine   	|
| gpsc2    	| science             	| X     	|             	| Gridengine   	|
| gpsc3    	| science             	| X     	|             	|              	|
| gpsc4    	| science             	| X     	|             	| Gridengine   	|
| gpscc2   	| collab.science      	| X     	|             	| Gridengine   	|
| ppp3     	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| ppp4     	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| banting  	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
| daley    	| science (ECCC only) 	| X     	|             	| PBS Pro      	|
